package com.example.smsforeva.smsinbox;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD_Inbox;
import com.example.smsforeva.database.Inbox;

public class SmsListViewInboxFragment extends Fragment {

	private View rootView;
	// private CursorAdapter adapter;
	private SimpleCursorAdapter adapter;
	TextView lblMsg, lblNo;
	ArrayAdapter<String> adapter2;
	ListView lvMsg;
	List<String> tes;
	List<Inbox> values;
	ArrayAdapter<Inbox> inboxs;
	public ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
	private CRUD_Inbox crud_i;
	Long timestamp;
	String date;
	String smsdate;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_smsinbox_listinbox,
				container, false);
		lvMsg = (ListView) rootView.findViewById(R.id.lvMsg);
		Uri inboxURI = Uri.parse("content://sms/inbox");
		String[] reqCols = new String[] { "_id", "address", "body", "date" };
		oslist = new ArrayList<HashMap<String, String>>();

		ContentResolver cr = getActivity().getBaseContext()
				.getContentResolver();
		Cursor c = cr.query(inboxURI, null, null, null, null);
		if (c.moveToFirst()) {
			for (int i = 0; i < c.getCount(); i++) {

				String id = c.getString(c.getColumnIndex("_id")).toString();
				String number = c.getString(c.getColumnIndex("address"))
						.toString();
				String body = c.getString(c.getColumnIndex("body")).toString();
				date = c.getString(c.getColumnIndex("date")).toString();
				timestamp = Long.parseLong(date);
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(timestamp);
				Date finaldate = calendar.getTime();
				smsdate = finaldate.toString();

				HashMap<String, String> map = new HashMap<String, String>();
				map.put("_id", id);
				map.put("address", number);
				map.put("body", body);
				map.put("date", smsdate);

				oslist.add(map);
				c.moveToNext();
				}
		}
				ListAdapter la = new SimpleAdapter(getActivity()
						.getBaseContext(), oslist, R.layout.row, new String[] {
						"address", "body", "_id", "date" }, new int[] {
						R.id.lblNumber, R.id.lblMsg, R.id.lblId, R.id.lbltime });

				/*
				 * adapter = new
				 * SimpleCursorAdapter(getActivity().getBaseContext(),
				 * R.layout.row, c, new String[] {
				 * "address","body","_id",smsdate }, new int[] {
				 * R.id.lblNumber,R.id.lblMsg,R.id.lblId,R.id.lbltime });
				 *//*
					 * ArrayList< String> list = new ArrayList<String>(); for
					 * (int i = 0; i < reqCols.length; i++) {
					 * list.add(reqCols[i]); }
					 */
				// adapter = new
				// adapter2 = new
				// ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,list
				// );

				lvMsg.setAdapter(la);

				lvMsg.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> adapter, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						String a = ((TextView) view.findViewById(R.id.lblMsg))
								.getText().toString();
						String b = ((TextView) view
								.findViewById(R.id.lblNumber)).getText()
								.toString();
						String c = ((TextView) view.findViewById(R.id.lblId))
								.getText().toString();
						//String d = ((TextView)view.findViewById(R.id.lbltime)).getText().toString();
						
						String d = date;
						//Long d = timestamp;
						Bundle bundle = new Bundle();
						bundle.putString("mesage", a);
						bundle.putString("number", b);
						bundle.putString("id", c);
						bundle.putString("time", d);
						SmsViewInboxFragment svif = new SmsViewInboxFragment();
						svif.setArguments(bundle);
						android.support.v4.app.FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						ft.replace(R.id.frame_container_smsinbox, svif);
						ft.addToBackStack(null);
						ft.commit();
					}
				});
		
		return rootView;
	}
}
