package com.example.smsforeva.smsinbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD;
import com.example.smsforeva.database.CRUD_Inout;
import com.example.smsforeva.database.CRUD_Outbox;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Group;
import com.example.smsforeva.database.Message;

public class SmsViewInboxFragment extends Fragment {
	private View rootView;
	String nomortelpon, namakontak, code;
	String mesage, number, id;
	long time;
	// TextView ta,tb,tc;
	String[] parts;
	String part1, part2, part3;
	int ids;
	String code_g, name_g;
	private ListView lv;
	private DiscussArrayAdapter daa;
	EditText mess;
	Button send;
	private CRUD_Inout crud;
	private CRUD_Outbox crud_c;
	private CRUD_Inout crud_chat;
	private List<Message> values;
	private ArrayAdapter<Message> adapter;
	public ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();
	LinearLayout wrapper;
	private Object rowview;
	View ls;
	int iidd;
	private static final String SMS_SEND_ACTION = "CTS_SMS_SEND_ACTION";
	private static final String SMS_DELIVERY_ACTION = "CTS_SMS_DELIVERY_ACTION";
	IntentFilter sendIntentFilter, receiveIntentFilter;
	PendingIntent sentPI, deliveredPI;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_smsinbox_viewinbox,
				container, false);
		//ls = inflater.inflate(R.layout.row_message, container, false);
		Bundle bundle = this.getArguments();
		if (bundle != null) {
			if (bundle.containsKey("mesage")) {
				String s = bundle.getString("mesage");
				mesage = s;
			}
			if (bundle.containsKey("number")) {
				String n = bundle.getString("number");
				number = n;
			}
			if (bundle.containsKey("id")) {
				String n = bundle.getString("id");
				id = n;
			}
			if (bundle.containsKey("time")) {
				String t = bundle.getString("time");
				time = Long.parseLong(t);
			}
		}
		oslist = new ArrayList<HashMap<String, String>>();
		sendIntentFilter = new IntentFilter(SMS_SEND_ACTION);
		receiveIntentFilter = new IntentFilter(SMS_DELIVERY_ACTION);
		lv = (ListView) rootView.findViewById(R.id.listinboxx);
		crud = new CRUD_Inout(getActivity().getBaseContext());
		crud.open();
		iidd = Integer.parseInt(id.toString());
		try {
			crud.createMessageIn( iidd,mesage, number, "from :", time + "");
		} catch (Exception e) {
			// TODO: handle exception
		}
		crud.close();
		additems(number);
		mess = (EditText) rootView.findViewById(R.id.txtsendmess);
		send = (Button) rootView.findViewById(R.id.btnsendmess);
		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// daa.add(new OneComment(false, mess.getText().toString()));
				String mes = mess.getText().toString();
				String nume = number;
				if (mes.equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(),
							"message text required!", Toast.LENGTH_SHORT)
							.show();
				} else {
					AlertDialog send = askSending();
					send.show();
				}
				//mess.setText("");
			}

			private AlertDialog askSending() {
				AlertDialog myQuittingDialogBox = new AlertDialog.Builder(
						getActivity())
						// set message, title, and icon
						.setTitle("Confirmation Send")
						.setMessage("Sending this message?")

						.setPositiveButton("Send",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int whichButton) {
									/*	Time now = new Time(Time.getCurrentTimezone());
										now.setToNow();
										String date = Long.toString(now.toMillis(false));
										crud_chat = new CRUD_Inout(getActivity().getBaseContext());
										crud_chat.open();
										crud_chat.createMessage(mess.getText().toString(),number,"to :",date);
										crud_chat.close();
										additems(number);*/
										/*String pesan = mess.getText().toString();
										Toast.makeText(getActivity(), pesan, Toast.LENGTH_SHORT).show();*/
										String nomor = number+";";
										String pesan = mess.getText()
												.toString();
										if (nomor.endsWith(";")) {
											nomor = nomor.substring(0,
													nomor.length() - 1);
											try {
												sendSMS(nomor, pesan);
												Toast.makeText(
														getActivity()
																.getBaseContext(),
														"on sending progress, wait message delivered",
														Toast.LENGTH_SHORT)
														.show();
											} catch (Exception e) {
												Toast.makeText(
														getActivity()
																.getBaseContext(),
														"Failed to send SMS",
														Toast.LENGTH_LONG)
														.show();
											}
										} else {
											try {
												sendSMS(nomor, pesan);
												Toast.makeText(
														getActivity()
																.getBaseContext(),
														"on sending progress, wait message delivered",
														Toast.LENGTH_SHORT)
														.show();
											} catch (Exception e) {
												// TODO: handle exception
												Toast.makeText(
														getActivity()
																.getBaseContext(),
														"Failed to send SMS",
														Toast.LENGTH_LONG)
														.show();
											}
										}
									}
								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								}).create();
				return myQuittingDialogBox;

			}

		});
		try {
			parts = mesage.split(" ");
			part1 = parts[0];
			part2 = parts[1];
			part3 = parts[2];
			if (part1.equalsIgnoreCase("REG")) {
				AlertDialog a = askoption();
				a.show();
				// Toast.makeText(getActivity(), part1+part2+part3,
				// Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return rootView;
	}

	private void additems(String M_number) {
		// TODO Auto-generated method stub
		// daa.add(new OneComment(true, mesage));

		oslist = new ArrayList<HashMap<String, String>>();
		crud.open();
		values = crud.findnumber(M_number);
		for (int i = 0; i < values.size(); i++) {
			String mess = values.get(i).getMessage();
			String status = values.get(i).getStatus();
			String id = values.get(i).getIdmes()+"";
			String notel = values.get(i).getNotelepon();
			// String time = values.get(i).getTime();
			// daa.add(new OneComment(true, mess));
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("message", mess);
			map.put("status", status);
			map.put("id", id);
			map.put("notel", notel);
			// map.put("time", time);
			oslist.add(map);
			/*
			 * if (status.equalsIgnoreCase("from :")) { wrapper = (LinearLayout)
			 * ls.findViewById(R.id.wrapper);
			 * wrapper.setBackgroundResource(R.drawable.bubble_green); }
			 */

		}
		;
		crud.close();
		ListAdapter la = new SimpleAdapter(getActivity().getBaseContext(),
				oslist, R.layout.row_message, new String[] { "message",
						"status", "id", "notel" }, new int[] { R.id.lblcomment,
						R.id.lblstatus, R.id.lblid_m, R.id.lbltime });
		lv.setAdapter(la);

	}

	private AlertDialog askoption() {
		// TODO Auto-generated method stub
		AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
				// set message, title, and icon
				.setTitle("Add Contact?")
				.setMessage("Do you want to add this contact?")

				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						CRUD crud = new CRUD(getActivity());
						crud.open();
						List<Group> values = crud.findGroup(part2);
						for (int i = 0; i < values.size(); i++) {
							ids = (int) values.get(i).getId();
							code_g = values.get(i).getCode();
							name_g = values.get(i).getName();
						}
						crud.close();
						String name = part3;
						CRUD_contact cruds = new CRUD_contact(getActivity());
						cruds.openn();
						cruds.createContact(ids, code_g, name_g, name, number,
								"", "", "");
						cruds.close();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).create();
		return myQuittingDialogBox;

	}
	private BroadcastReceiver smsReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			switch (getResultCode()) {
			case Activity.RESULT_OK:
				Toast.makeText(getActivity().getBaseContext(), "SMS sent",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				Toast.makeText(getActivity().getBaseContext(),
						"Generic failure", Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_NO_SERVICE:
				Toast.makeText(getActivity().getBaseContext(), "No service",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_NULL_PDU:
				Toast.makeText(getActivity().getBaseContext(), "Null PDU",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				Toast.makeText(getActivity().getBaseContext(), "Radio off",
						Toast.LENGTH_SHORT).show();
				break;
			default:
				// sent SMS message failed
				Toast.makeText(getActivity().getBaseContext(),
						"Failed to send SMS", Toast.LENGTH_LONG).show();
				break;
			}
			getActivity().unregisterReceiver(this);
		}

	};
	private BroadcastReceiver deliverdReceiver = new BroadcastReceiver() {

		

		

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			switch (getResultCode()) {
			case Activity.RESULT_OK:
				Toast.makeText(getActivity().getBaseContext(), "SMS delivered",
						Toast.LENGTH_SHORT).show();
				Time now = new Time(Time.getCurrentTimezone());
				now.setToNow();
				String date = Long.toString(now.toMillis(false));
				//OUTBOX
				crud_c = new CRUD_Outbox(getActivity().getBaseContext());
				crud_c.open();
				crud_c.createOutbox(date, mess.getText().toString(), number);
				crud_c.close();
				//CHAT
				crud_chat = new CRUD_Inout(getActivity().getBaseContext());
				crud_chat.open();
				crud_chat.createMessage(mess.getText().toString(),number,"to :",date);
				crud_chat.close();
				additems(number);
				break;
			case Activity.RESULT_CANCELED:
				Toast.makeText(getActivity().getBaseContext(),
						"SMS not delivered", Toast.LENGTH_SHORT).show();
				break;
			}
			getActivity().unregisterReceiver(this);
		}
	};
public void sendSMS(String phoneN, String messageS) {
		
		sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(
				SMS_SEND_ACTION), 0);

		deliveredPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(
				SMS_DELIVERY_ACTION), 0);

		getActivity().registerReceiver(deliverdReceiver, sendIntentFilter);
		getActivity().registerReceiver(smsReceiver, receiveIntentFilter);
		SmsManager sms = SmsManager.getDefault();
		ArrayList<String> parts = sms.divideMessage(messageS);
		ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();
		for (int i = 0; i < parts.size(); i++) {
			sentIntents.add(PendingIntent.getBroadcast(getActivity()
					.getBaseContext(), 0, new Intent(SMS_SEND_ACTION), 0));
			deliveryIntents.add(PendingIntent.getBroadcast(getActivity()
					.getBaseContext(), 0, new Intent(SMS_DELIVERY_ACTION), 0));
		}
		StringTokenizer st = new StringTokenizer(phoneN, ";");
		while (st.hasMoreElements()) {
			String temp = (String) st.nextElement();
			if (temp.length() >= 2 && messageS.trim().length() > 160) {
				sms.sendMultipartTextMessage(temp, null, parts, sentIntents,
						deliveryIntents);
				
			} else if ((temp.length() > 0 && temp.length() < 2)
					&& (messageS.trim().length() > 0 && messageS.trim()
							.length() <= 160)) {
				sms.sendTextMessage(phoneN, null, messageS, sentPI, deliveredPI);
		
			} else if ((temp.length() > 0 && temp.length() < 2)
					&& (messageS.trim().length() > 160)) {
				sms.sendMultipartTextMessage(phoneN, null, parts, sentIntents,
						deliveryIntents);
		
			} else if ((temp.length() >= 2)
					&& (messageS.trim().length() > 0 && messageS.trim()
							.length() <= 160)) {
				sms.sendTextMessage(temp, null, messageS, sentPI, deliveredPI);
				} else {

				Toast.makeText(getActivity().getBaseContext(),
						"Please enter both phone number and message.",
						Toast.LENGTH_SHORT).show();
			}
			
		}
	}

}
