package com.example.smsforeva.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.smsforeva.fragments.BroadcastFragment;
import com.example.smsforeva.fragments.ContactFragment;
import com.example.smsforeva.fragments.GroupFragment;
import com.example.smsforeva.fragments.SmsFragment;
import com.example.smsforeva.fragments.SmsInboxFragment;
import com.example.smsforeva.fragments.SmsOutboxFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Top Rated fragment activity
			return new ContactFragment();
		case 1:
			// Games fragment activity
			return new GroupFragment();
		case 2:
			// Movies fragment activity
			return new SmsFragment();
		case 3:
			// Movies fragment activity
			return new BroadcastFragment();
		case 4:
			return new SmsInboxFragment();
		case 5:
			return new SmsOutboxFragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 6;
	}

}
