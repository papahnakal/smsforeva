package com.example.smsforeva.group;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD;
import com.example.smsforeva.database.Group;

public class listGroupFragment extends Fragment{
	private View rootView;
	Button addGroup;
	ListView list;
	TextView find;
	ArrayAdapter<Group> adapter;
	List<Group>values;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			rootView= inflater.inflate(R.layout.fragment_group_listgroup, container, false);
			setHasOptionsMenu(true);
			addGroup = (Button)rootView.findViewById(R.id.btn_addGroup);
			addGroup.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					addGroupFragment newFragment = new addGroupFragment();
					FragmentTransaction transaction = getFragmentManager().beginTransaction();
					transaction.replace(R.id.frame_container_group, newFragment);
					transaction.addToBackStack(null);
					transaction.commit();
				//	transaction.hide(GroupFragment.this);
					
				}
			});
		list =(ListView)rootView.findViewById(R.id.listview_group);
		CRUD crud = new CRUD(getActivity());
		crud.open();
		values = crud.getAllGroup();
		adapter = new ArrayAdapter<Group>(getActivity(), android.R.layout.simple_list_item_1,values);
		list.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		crud.close();
		find = (TextView)rootView.findViewById(R.id.inputSearch);
		find.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			listGroupFragment.this.adapter.getFilter().filter(arg0);
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				//Toast.makeText(getActivity(), position+"", Toast.LENGTH_SHORT).show();
				 CRUD group = new CRUD(getActivity().getBaseContext());
			     group.open();
				long id_g= values.get(position).getId();
				String code = values.get(position).getCode();
				String  name = values.get(position).getName();
				String note = values.get(position).getNote();
				viewGroupFragment vgf = new viewGroupFragment();
				Bundle bundle = new Bundle();
				bundle.putLong("id_g", id_g);
				bundle.putString("code", code);
				bundle.putString("name", name);
				bundle.putString("note", note);
				group.close();
				vgf.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.frame_container_group, vgf);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		
			
	return rootView;	 
	}
	 public void onBackPressed() {
	        //Display alert message when back button has been pressed
	        back();
	        return;
	    }
	private void back() {
		// TODO Auto-generated method stub
	/*	Intent intent = new Intent(Intent.ACTION_MAIN);
     intent.addCategory(Intent.CATEGORY_HOME);
     intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
     startActivity(intent);*/
     getActivity().finish();
	}

}
