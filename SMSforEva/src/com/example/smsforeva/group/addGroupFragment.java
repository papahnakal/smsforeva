package com.example.smsforeva.group;

import java.util.List;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD;
import com.example.smsforeva.database.Group;
import com.example.smsforeva.fragments.GroupFragment;


public class addGroupFragment extends Fragment{
	private View rootView;
	Button insGroup;
	EditText ET_codeGroup,ET_nameGroup,ET_noteGroup;
	List<Group>values;
	private CRUD crud;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			rootView= inflater.inflate(R.layout.fragment_group_addgroup, container, false);
			ET_codeGroup = (EditText)rootView.findViewById(R.id.Ins_CodeGroup);
			ET_nameGroup = (EditText)rootView.findViewById(R.id.Ins_GroupName);
			ET_noteGroup = (EditText)rootView.findViewById(R.id.Ins_noteGroup);
			insGroup = (Button)rootView.findViewById(R.id.btnIns_group);
			insGroup.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					String a = ET_codeGroup.getText().toString();
					String b = ET_nameGroup.getText().toString();
					String c = ET_noteGroup.getText().toString();
					try {
						if(a.equalsIgnoreCase("")&&b.equalsIgnoreCase("")){
							Toast.makeText(getActivity().getBaseContext(), "Group code and group name is required!", Toast.LENGTH_SHORT).show();
						}else if(a.equalsIgnoreCase("")){
							Toast.makeText(getActivity().getBaseContext(), "Group code is required!", Toast.LENGTH_SHORT).show();
						}else if(b.equalsIgnoreCase("")){
							Toast.makeText(getActivity().getBaseContext(), "Group name is required!", Toast.LENGTH_SHORT).show();
						}else{
							/*Group ga = new GroupActivity();
							ga.finish();*/
							crud = new CRUD(getActivity().getBaseContext());
							crud.open();
							crud.createGroup(a, b, c);
							crud.close();
						/*	Intent intent = new Intent(getActivity(),GroupFragment.class);
							startActivity(intent);*/
							Toast.makeText(getActivity().getBaseContext(), "Group has been created!", Toast.LENGTH_SHORT).show();
							GroupFragment newFragment = new GroupFragment();
							android.support.v4.app.FragmentManager fm=getFragmentManager();
							if(fm.getBackStackEntryCount()>0){
								fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
							}
							FragmentTransaction transaction = getFragmentManager().beginTransaction();
							transaction.replace(R.id.frame_container_group, newFragment);
							//transaction.addToBackStack(null);
							transaction.commit();
							//fg.reloadList();
							
							/*Fragment fm = new Fragment();
							fm = new FGroup();
							fm.getActivity();*/
						}
				
					} catch (Exception e) {
						// TODO: handle exception
					}
			
				}
			});
	return rootView;	 
	}
}
