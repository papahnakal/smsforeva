package com.example.smsforeva.group;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD;

public class viewGroupFragment extends Fragment{
	private View rootView;
	private String code;
	private String name;
	private String note;
	private long id_g;
	private TextView t_code;
	private TextView t_name;
	private TextView t_note;
	private Button delete, update;
	ListView list;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			Bundle bundle =this.getArguments();
			id_g = bundle.getLong("id_g");
			code = bundle.getString("code");
			name = bundle.getString("name");
			note = bundle.getString("note");
			rootView= inflater.inflate(R.layout.fragment_group_viewgroup, container, false);
			t_code =(TextView)rootView.findViewById(R.id.t_code);
			t_name =(TextView)rootView.findViewById(R.id.t_name);
			t_note =(TextView)rootView.findViewById(R.id.t_note);
			
			t_code.setText(code);
			t_name.setText(name);
			t_note.setText(note);
			
			delete = (Button)rootView.findViewById(R.id.btn_deleteGroup);
			update=(Button)rootView.findViewById(R.id.btn_updateGroup);
			
			update.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if 	(code.equalsIgnoreCase("001")
							&& name.equalsIgnoreCase("All")){
					Toast.makeText(getActivity().getBaseContext(),
							"can't update this Group!",
							Toast.LENGTH_SHORT).show();
				}else{
					if (t_code.getText().toString().equalsIgnoreCase("")
							&& t_name.getText().toString().equalsIgnoreCase("")) {
						Toast.makeText(getActivity().getBaseContext(),
								"Group code and group name is required!",
								Toast.LENGTH_SHORT).show();
					} else if (t_code.getText().toString().equalsIgnoreCase("")) {
						Toast.makeText(getActivity().getBaseContext(), "Group code is required!",
								Toast.LENGTH_SHORT).show();
					} else if (t_name.getText().toString().equalsIgnoreCase("")) {
						Toast.makeText(getActivity().getBaseContext(), "Group name is required!!",
								Toast.LENGTH_SHORT).show();
					} else {

						AlertDialog a = askupdate();
						a.show();
					}
				}

				}

				private AlertDialog askupdate() {
					AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
					// set message, title, and icon
					.setTitle("Update?")
					.setMessage("Do you want to update this group?")

					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int whichButton) {
									// your deleting code
									/*GroupActivity ga = new GroupActivity();
									ga.finish();*/
									try {

										CRUD crud = new CRUD(getActivity().getBaseContext());
										crud.open();
										crud.updateGroup(id_g, t_code.getText()
												.toString(), t_name.getText()
												.toString(), t_note.getText()
												.toString());
										// ffg = new FGroup();
										// ffg.reloadList();
										crud.close();
										dialog.dismiss();
										Toast.makeText(getActivity().getBaseContext(),
												"Group has been updated!",
												Toast.LENGTH_SHORT).show();
										
										android.support.v4.app.FragmentManager fm=getFragmentManager();
										if(fm.getBackStackEntryCount()>0){
											fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
										}else{
											listGroupFragment gf = new listGroupFragment();
											FragmentTransaction transaction = getFragmentManager().beginTransaction();
											transaction.replace(R.id.frame_container_group, gf);
											//transaction.addToBackStack(null);
											transaction.commit();
										}
										
										/*
										 * Fragment fm = null; fm = new FGroup();
										 * fm.getActivity();
										 */
										//Vgroup.this.finish();

									} catch (Exception e) {
										// TODO: handle exception
									}

								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).create();
			return myQuittingDialogBox;
				}
				
			});
			
			delete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					if 	(code.equalsIgnoreCase("001")
							&& name.equalsIgnoreCase("All")){
					Toast.makeText(getActivity().getBaseContext(),
							"can't delete this Group!",
							Toast.LENGTH_SHORT).show();
				}else{
					
					AlertDialog a = AskOption();
					a.show();
				}
				}

				private AlertDialog AskOption() {
					// TODO Auto-generated method stub
					AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
					// set message, title, and icon
					.setTitle("Delete Group?")
					.setMessage("Do you want to delete this group?")

					.setPositiveButton("Delete",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int whichButton) {
									// your deleting code
									/*GroupActivity ga = new GroupActivity();
									ga.finish();*/
								
									CRUD crud = new CRUD(getActivity().getBaseContext());
									crud.open();
									crud.deleteGroup(id_g);
									String code ="001";
									String name ="All";
									int ganti=0;
									crud.updateGroupContact((int)id_g,ganti, code, name);
									// ffg = new FGroup();
									// ffg.reloadList();
									crud.close();
									dialog.dismiss();
									Toast.makeText(getActivity().getBaseContext(),
											"Group has been deleted!",
											Toast.LENGTH_SHORT).show();
									android.support.v4.app.FragmentManager fm=getFragmentManager();
									if(fm.getBackStackEntryCount()>0){
										fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
									}else{
										listGroupFragment gf = new listGroupFragment();
										
										FragmentTransaction transaction = getFragmentManager().beginTransaction();
										transaction.replace(R.id.frame_container_group,gf );
										//transaction.addToBackStack(null);
										transaction.commit();
									
									}
									
									/*Intent a = new Intent(getActivity().getBaseContext(),
											.class);
									startActivity(a);*/
									/*
									 * Fragment fm = null; fm = new FGroup();
									 * fm.getActivity();
									 */
									//Vgroup.this.finish();
								}
							})
					.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).create();
			return myQuittingDialogBox;

					
				}
			});
			return rootView;
}
	}
