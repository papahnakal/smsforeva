package com.example.smsforeva.contact;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Contact;

public class listContactFragment extends Fragment{
	private View rootView;
	ListView list;
	TextView findcontact;
	ArrayAdapter<Contact> adapter;
	CRUD_contact crud;
	List<Contact> values;
	Button addC;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_contact_listcontact, container, false);
		 list = (ListView) rootView.findViewById(R.id.listview_contact);
			addC = (Button)rootView.findViewById(R.id.btn_addCont);
		 addC.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addContactFragment newFragment = new addContactFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.frame_container_contact, newFragment);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		 crud = new CRUD_contact(getActivity().getBaseContext());
		 crud.openn();
		 values = crud.getFull();
		 adapter = new ArrayAdapter<Contact>(getActivity().getBaseContext(),android.R.layout.simple_list_item_1,values );
		 list.setAdapter(adapter);
		 adapter.notifyDataSetChanged();
		 crud.close();
		 findcontact =(TextView)rootView.findViewById(R.id.contactSearct);
		 findcontact.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
			listContactFragment.this.adapter.getFilter().filter(arg0);	
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		 list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				//Toast.makeText(getActivity(),arg2+"" , Toast.LENGTH_SHORT).show();
				crud.openn();
				long id_c = values.get(position).getId();
				int id_g = values.get(position).getGroup_id();
				String G_code = values.get(position).getCode();
				String G_name = values.get(position).getName();
				String name = values.get(position).getName_contact();
				String phone = values.get(position).getPhone();
				String positions = values.get(position).getPosition();
				String address = values.get(position).getAddress();
				String note = values.get(position).getNote_contact();
				viewContactFragment vcf = new viewContactFragment();
				Bundle bundle = new Bundle();
				bundle.putLong("id_c", id_c);
				bundle.putInt("id_g", id_g);
				bundle.putString("G_code", G_code);
				bundle.putString("G_name", G_name);
				bundle.putString("name", name);
				bundle.putString("phone", phone);
				bundle.putString("positions", positions);
				bundle.putString("address", address);
				bundle.putString("note", note);
				crud.close();
				vcf.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.frame_container_contact, vcf);
				transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		 return rootView;	 
	}
}
