package com.example.smsforeva.contact;

import java.util.List;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Group;


public class viewContactFragment extends Fragment{
	private View rootView;
	TextView b;
	EditText c, d, e, f, g;
	Spinner spin;
	List<Group> values;
	Button delete,update;
	ArrayAdapter<Group> groups;
	long id_c;
	String G_code, G_name;
	int id_g;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Bundle bundle = this.getArguments();
		id_c=bundle.getLong("id_c");
		id_g=bundle.getInt("id_g");
		G_code =bundle.getString("G_code");
		G_name =bundle.getString("G_name");
		String name = bundle.getString("name");
		String phone = bundle.getString("phone");
		String positions =bundle.getString("positions");
		String address = bundle.getString("address");
		String note = bundle.getString("note");
		rootView= inflater.inflate(R.layout.fragment_contact_viewcontact, container, false);
		spin =(Spinner)rootView.findViewById(R.id.spinnerContact);
		CRUD crud = new CRUD(getActivity().getBaseContext());
		crud.open();
		values = crud.getAllGroup();
		groups = new ArrayAdapter<Group>(getActivity().getBaseContext(), android.R.layout.simple_spinner_item,values);
		groups.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin.setAdapter(groups);
		crud.close();
		spin.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				CRUD crud = new CRUD(getActivity().getBaseContext());
				crud.open();
				id_g = (int) values.get(position).getId();
				G_code = values.get(position).getCode();
				G_name = values.get(position).getName();
				crud.close();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		c = (EditText) rootView.findViewById(R.id.txt_ce_name);
		d = (EditText)rootView.findViewById(R.id.txt_ce_phone);
		e = (EditText) rootView.findViewById(R.id.txt_ce_position);
		f = (EditText) rootView.findViewById(R.id.txt_ce_address);
		g = (EditText) rootView.findViewById(R.id.txt_ce_note);
		// a.setText("id contact : "+id_c+"");
		// b.setText("id group : "+id_g+"");
		c.setText(name);
		d.setText(phone);
		e.setText(positions);
		f.setText(address);
		g.setText(note);
		
		delete = (Button)rootView.findViewById(R.id.btn_deleteContact);
		update =(Button)rootView.findViewById(R.id.btn_updateContact);
		update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (c.getText().toString().equalsIgnoreCase("")
						&& d.getText().toString().equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(),
							"name and  number phone is required!",
							Toast.LENGTH_SHORT).show();
				} else if (c.getText().toString().equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(), "name is required!",
							Toast.LENGTH_SHORT).show();
				} else if (d.getText().toString().equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(), "Number phone is required!!",
							Toast.LENGTH_SHORT).show();
				} else {

					AlertDialog ad = askupdate();
					ad.show();
				}	
			}

			private AlertDialog askupdate() {
				AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
				// set message, title, and icon
				.setTitle("Update?")
				.setMessage("Do you want to update this group?")

				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// your deleting code
							/*	GroupActivity ga = new GroupActivity();
								ga.finish();*/
								try {

									CRUD_contact crud = new CRUD_contact(
											getActivity().getBaseContext());
									crud.openn();
									crud.updateContact(id_c, id_g, G_code,
											G_name, c.getText().toString(), d
													.getText().toString(), e
													.getText().toString(), f
													.getText().toString(), g
													.getText().toString());
									// ffg = new FGroup();
									// ffg.reloadList();
									crud.close();
									dialog.dismiss();
									Toast.makeText(getActivity().getBaseContext(),
											"Contact has been updated!",
											Toast.LENGTH_SHORT).show();
									android.support.v4.app.FragmentManager fm=getFragmentManager();
									if(fm.getBackStackEntryCount()>0){
										fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
									}else{
										listContactFragment gf = new listContactFragment();
										FragmentTransaction transaction = getFragmentManager().beginTransaction();
										transaction.replace(R.id.frame_container_contact, gf);
										//transaction.addToBackStack(null);
										transaction.commit();
									}
									
									/*
									 * Fragment fm = null; fm = new FGroup();
									 * fm.getActivity();
									 */
									//Vcontact.this.finish();

								} catch (Exception e) {
									// TODO: handle exception
								}

							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).create();
		return myQuittingDialogBox;

			}
		});
		delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				AlertDialog a = AskOption();
				a.show();
			}

			private AlertDialog AskOption() {
				AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
				// set message, title, and icon
				.setTitle("Delete Contact?")
				.setMessage("Do you want to delete this contact?")

				.setPositiveButton("Delete",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// your deleting code
								/*ContactActivity ca = new ContactActivity();
								ca.finish();*/
								CRUD_contact crud = new CRUD_contact(
										getActivity().getBaseContext());
								crud.openn();
								crud.deleteContact(id_c);
								// ffg = new FGroup();
								// ffg.reloadList();
								crud.close();
								dialog.dismiss();
								Toast.makeText(getActivity().getBaseContext(),
										"Contact has been deleted!",
										Toast.LENGTH_SHORT).show();
							
								android.support.v4.app.FragmentManager fm=getFragmentManager();
								if(fm.getBackStackEntryCount()>0){
									fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
								}else{
									listContactFragment gf = new listContactFragment();
									FragmentTransaction transaction = getFragmentManager().beginTransaction();
									transaction.replace(R.id.frame_container_contact, gf);
									//transaction.addToBackStack(null);
									transaction.commit();
								}
								
								/*
								 * Fragment fm = null; fm = new FGroup();
								 * fm.getActivity();
								 */
								//getActivity().finish();
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).create();
		return myQuittingDialogBox;

			}
		});
		
		
		
		
		return rootView;
	}
}
