package com.example.smsforeva.contact;

import java.util.List;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Group;

public class addContactFragment extends Fragment{
	private View rootView;
	Spinner spin;
	EditText name, phone, position, address, note;
	Button insert_contact;
	List<Group> values;
	ArrayAdapter<Group> groups;
	private CRUD_contact crud_c;
	long ids;
	String namegroup,codegroup,notegroup;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			rootView= inflater.inflate(R.layout.fragment_contact_addcontact, container, false);
			name = (EditText) rootView.findViewById(R.id.Ins_name);
			phone = (EditText) rootView.findViewById(R.id.Ins_phone);
			position = (EditText) rootView.findViewById(R.id.Ins_position);
			address = (EditText) rootView.findViewById(R.id.Ins_Address);
			note = (EditText) rootView.findViewById(R.id.Ins_note);
			spin = (Spinner)rootView. findViewById(R.id.spinner1);
			CRUD crud = new CRUD(getActivity().getBaseContext());
			crud.open();
			values = crud.getAllGroup();
			groups = new ArrayAdapter<Group>(getActivity().getBaseContext(), android.R.layout.simple_spinner_item,values);
			groups.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spin.setAdapter(groups);
			crud.close();
			spin.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					CRUD crud = new CRUD(getActivity().getBaseContext());
					crud.open();
					ids = values.get(position).getId();
					namegroup =values.get(position).getName();
					codegroup = values.get(position).getCode();
					notegroup = values.get(position).getNote();
					crud.close();
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			insert_contact = (Button)rootView.findViewById(R.id.btnIns_contact);
			insert_contact.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					int groupid = (int) ids;
					String G_name = namegroup;
					String G_code =codegroup;
					String a = name.getText().toString();
					String b = phone.getText().toString();
					String c = position.getText().toString();
					String d = address.getText().toString();
					String e = note.getText().toString();
					try {
						if (a.equalsIgnoreCase("") && b.equalsIgnoreCase("")) {
							Toast.makeText(getActivity().getBaseContext(),
									"Name contact and phone number required!",
									Toast.LENGTH_SHORT).show();
						}else if(a.equalsIgnoreCase("")){
							Toast.makeText(getActivity().getBaseContext(),
									"Name contact required!",
									Toast.LENGTH_SHORT).show();
						}else if(b.equalsIgnoreCase("")){
							Toast.makeText(getActivity().getBaseContext(),
									"Phone number required!",
									Toast.LENGTH_SHORT).show();
						} else {
							/*ContactActivity ca = new ContactActivity();
							ca.finish();*/
							crud_c = new CRUD_contact(getActivity().getBaseContext());
							crud_c.openn();
							crud_c.createContact(groupid,G_code,G_name, a, b, c, d, e);
							crud_c.close();
							Toast.makeText(getActivity().getBaseContext(),
									"Contact has been created!", Toast.LENGTH_SHORT)
									.show();
							listContactFragment newFragment = new listContactFragment();
							android.support.v4.app.FragmentManager fm=getFragmentManager();
							if(fm.getBackStackEntryCount()>0){
								fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
							}
							FragmentTransaction transaction = getFragmentManager().beginTransaction();
							transaction.replace(R.id.frame_container_contact, newFragment);
							//transaction.addToBackStack(null);
							transaction.commit();
							
							//addContact.this.finish();
						}
					} catch (Exception e2) {
						// TODO: handle exception
					}
				}
			});
			return rootView;
	}
}
