package com.example.smsforeva.sms;

import java.util.List;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Contact;
import com.example.smsforeva.fragments.SmsFragment;

public class chooseContactFragment extends Fragment {
	private View rootView;
	ListView list;
	TextView findcontact;
	ArrayAdapter<Contact> adapter;
	CRUD_contact crud;
	List<Contact> values;
	Button ok,back;
	String s="";
	String n="";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_sms_choosecontact,
				container, false);
		back = (Button)rootView.findViewById(R.id.btn_getBackC);
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			smsSingleFragment sf = new smsSingleFragment();
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(R.id.frame_container_sms, sf);
			transaction.commit();
			}
		});
		list = (ListView) rootView.findViewById(R.id.listview_choosecontact);
		
		crud = new CRUD_contact(getActivity().getBaseContext());
		crud.openn();
		values = crud.getFull();
		/*adapter = new ArrayAdapter<Contact>(getBaseContext(),
				android.R.layout.simple_list_item_1, values);*/
		adapter = new ArrayAdapter<Contact>(getActivity().getBaseContext(), android.R.layout.simple_list_item_multiple_choice,values);
		list.setAdapter(adapter);
		list.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		list.setTextFilterEnabled(true);
		adapter.notifyDataSetChanged();
		crud.close();
		ok = (Button)rootView.findViewById(R.id.btn_getCon);
ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				/*smsaciviy ss = new smsaciviy();
				ss.finish();*/
				// TODO Auto-generated method stub
				/*Intent intent = new Intent(Choosecontact.this,smsaciviy.class);
				intent.putExtra("no", s);
				intent.putExtra("na", n);
				startActivity(intent);
				Choosecontact.this.finish();*/
				//Toast.makeText(getActivity().getBaseContext(), s, Toast.LENGTH_SHORT).show();
				Bundle bundle = new Bundle();
				bundle.putString("no", s);
				bundle.putString("na", n);
				smsSingleFragment sf= new smsSingleFragment();
				SmsFragment ssf = new SmsFragment();
				ssf.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.frame_container_sms, ssf);
				
				transaction.attach(ssf);
				transaction.remove(sf);
				//transaction.addToBackStack(null);
				transaction.commit();
				//FragmentManager fm = getActivity().getFragmentManager();
				FragmentManager fm=getActivity().getFragmentManager();
				if(fm.getBackStackEntryCount()>0){
				fm.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}
				}
			
				
			
				
			
		});
findcontact = (TextView) rootView.findViewById(R.id.contactSrc);
findcontact.addTextChangedListener(new TextWatcher() {

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		chooseContactFragment.this.adapter.getFilter().filter(arg0);
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1,
			int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterTextChanged(Editable arg0) {
		// TODO Auto-generated method stub

	}
});
list.setOnItemClickListener(new OnItemClickListener() {

	@Override
	public void onItemClick(AdapterView<?> parent, View view,
			int position, long id) {
		// TODO Auto-generated method stub
		String ps ="";
		String pn="";
		int len =parent.getCount();
		SparseBooleanArray checked =list.getCheckedItemPositions();
		int checkeditemscount = checked.size();
		for (int i = 0; i < checkeditemscount; i++) {
			/*if(checked.get(i)){
				String no = values.get(i).getPhone();
				String na = values.get(i).getName_contact();
				ps = ps+no+";";
				pn = pn+na+";";
				s=ps;
				n=pn;
			}*/
			int posisi =checked.keyAt(i);
			if(checked.valueAt(i)){
				String no =values.get(posisi).getPhone();
				String na =values.get(posisi).getName_contact();
				ps =ps+no+";";
				pn =pn+na+";";
				
			}
				
		}//Toast.makeText(Choosecontact.this, s, Toast.LENGTH_SHORT).show();
		s=ps;
		n=pn;
	}

});

		return rootView;
	}
}
