package com.example.smsforeva.sms;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD_Inout;
import com.example.smsforeva.database.CRUD_Outbox;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Contact;

public class smsSingleFragment extends Fragment {
	EditText to;
	EditText message;
	EditText names;
	String phone;
	Spinner spin;
	String s;
	String n;
	AutoCompleteTextView autotex;
	ArrayAdapter<Contact> adapter;
	CRUD_contact crud;
	List<Contact> values;
	Button btnadd, send;

	IntentFilter sendIntentFilter, receiveIntentFilter;
	/*
	 * String SENT = "SMS_SENT"; String DELIVERED = "SMS_DELIVERED";
	 */
	PendingIntent sentPI, deliveredPI;
	// BroadcastReceiver smsSentReceiver, smsDeliveredReceiver;
	private View rootView;
	private static final String SMS_SEND_ACTION = "CTS_SMS_SEND_ACTION";
	private static final String SMS_DELIVERY_ACTION = "CTS_SMS_DELIVERY_ACTION";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_sms_single, container,
				false);
		to = (EditText) rootView.findViewById(R.id.txt_typenumber);
		names = (EditText) rootView.findViewById(R.id.txt_name_cont);
		message = (EditText) rootView.findViewById(R.id.txt_typemessage);
		Bundle bundle2 = this.getArguments();

		if (bundle2 != null) {
			if (bundle2.containsKey("no")) {
				String s = bundle2.getString("no");
				to.setText(s);
			}
			if (bundle2.containsKey("na")) {
				String n = bundle2.getString("na");
				names.setText(n);
			}

		} else {

			// Toast.makeText(getActivity().getBaseContext(), "null",
			// Toast.LENGTH_SHORT).show();
		}

		send = (Button) rootView.findViewById(R.id.btn_send_mess);
		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String nomor = to.getText().toString();
				String pesan = message.getText().toString();
				if (nomor.equalsIgnoreCase("") && pesan.equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(),
							"number phone and message text required!",
							Toast.LENGTH_SHORT).show();
				} else if (nomor.equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(),
							"number phone required!", Toast.LENGTH_SHORT)
							.show();
				} else if (pesan.equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(),
							"message text required!", Toast.LENGTH_SHORT)
							.show();
				} else {

					AlertDialog a = AskOption();
					a.show();

				}
			}

			private AlertDialog AskOption() {
				AlertDialog myQuittingDialogBox = new AlertDialog.Builder(
						getActivity())
						// set message, title, and icon
						.setTitle("Confirmation Send")
						.setMessage("Sending this message?")

						.setPositiveButton("Send",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int whichButton) {
										String nomor = to.getText().toString();
										String pesan = message.getText()
												.toString();
										if (nomor.endsWith(";")) {
											nomor = nomor.substring(0,
													nomor.length() - 1);
											try {
												sendSMS(nomor, pesan);
												Toast.makeText(getActivity().getBaseContext(),
														"on sending progress, wait message delivered",
														Toast.LENGTH_SHORT).show();
											} catch (Exception e) {
												Toast.makeText(
														getActivity()
																.getBaseContext(),
														"Failed to send SMS",
														Toast.LENGTH_LONG)
														.show();
											}
										} else {
											try {
												sendSMS(nomor, pesan);
												Toast.makeText(getActivity().getBaseContext(),
														"on sending progress, wait message delivered",
														Toast.LENGTH_SHORT).show();
											} catch (Exception e) {
												// TODO: handle exception
												Toast.makeText(
														getActivity()
																.getBaseContext(),
														"Failed to send SMS",
														Toast.LENGTH_LONG)
														.show();
											}
										}
									}
								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								}).create();
				return myQuittingDialogBox;

			}
		});

		btnadd = (Button) rootView.findViewById(R.id.btn_chose_cont);
		btnadd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/*
				 * Intent in = new
				 * Intent(getActivity().getBaseContext(),Choosecontact.class);
				 * startActivity(in);
				 */
				chooseContactFragment ccf = new chooseContactFragment();
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				transaction.replace(R.id.frame_container_sms, ccf);
				// transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		sendIntentFilter = new IntentFilter(SMS_SEND_ACTION);
		receiveIntentFilter = new IntentFilter(SMS_DELIVERY_ACTION);
		crud = new CRUD_contact(getActivity().getBaseContext());
		crud.openn();
		values = crud.getFull();
		adapter = new ArrayAdapter<Contact>(getActivity().getBaseContext(),
				android.R.layout.simple_dropdown_item_1line, values);

		crud.close();

		return rootView;
	}

	private BroadcastReceiver smsReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			switch (getResultCode()) {
			case Activity.RESULT_OK:
				Toast.makeText(getActivity().getBaseContext(), "SMS sent",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				Toast.makeText(getActivity().getBaseContext(),
						"Generic failure", Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_NO_SERVICE:
				Toast.makeText(getActivity().getBaseContext(), "No service",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_NULL_PDU:
				Toast.makeText(getActivity().getBaseContext(), "Null PDU",
						Toast.LENGTH_SHORT).show();
				break;
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				Toast.makeText(getActivity().getBaseContext(), "Radio off",
						Toast.LENGTH_SHORT).show();
				break;
			default:
				// sent SMS message failed
				Toast.makeText(getActivity().getBaseContext(),
						"Failed to send SMS", Toast.LENGTH_LONG).show();
				break;
			}
			getActivity().unregisterReceiver(this);
		}

	};
	private BroadcastReceiver deliverdReceiver = new BroadcastReceiver() {

		private CRUD_Inout crud_chat;

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			switch (getResultCode()) {
			case Activity.RESULT_OK:
				Toast.makeText(getActivity().getBaseContext(), "SMS delivered",
						Toast.LENGTH_SHORT).show();
				Time now = new Time(Time.getCurrentTimezone());
				now.setToNow();
				String date = Long.toString(now.toMillis(false));
				crud_c = new CRUD_Outbox(getActivity().getBaseContext());
				crud_c.open();
				crud_c.createOutbox(date, message.getText().toString(), to.getText().toString());
				crud_c.close();
				crud_chat = new CRUD_Inout(getActivity().getBaseContext());
				crud_chat.open();
				crud_chat.createMessage(message.getText().toString(),to.getText().toString(),"to :",date);
				crud_chat.close();
				break;
			case Activity.RESULT_CANCELED:
				Toast.makeText(getActivity().getBaseContext(),
						"SMS not delivered", Toast.LENGTH_SHORT).show();
				break;
			}
			getActivity().unregisterReceiver(this);
		}
	};
	private CRUD_Outbox crud_c;

	public void sendSMS(String phoneN, String messageS) {
		
		sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(
				SMS_SEND_ACTION), 0);

		deliveredPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(
				SMS_DELIVERY_ACTION), 0);

		getActivity().registerReceiver(deliverdReceiver, sendIntentFilter);
		getActivity().registerReceiver(smsReceiver, receiveIntentFilter);
		SmsManager sms = SmsManager.getDefault();
		ArrayList<String> parts = sms.divideMessage(messageS);
		ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();
		for (int i = 0; i < parts.size(); i++) {
			sentIntents.add(PendingIntent.getBroadcast(getActivity()
					.getBaseContext(), 0, new Intent(SMS_SEND_ACTION), 0));
			deliveryIntents.add(PendingIntent.getBroadcast(getActivity()
					.getBaseContext(), 0, new Intent(SMS_DELIVERY_ACTION), 0));
		}
		StringTokenizer st = new StringTokenizer(phoneN, ";");
		while (st.hasMoreElements()) {
			String temp = (String) st.nextElement();
			if (temp.length() >= 2 && messageS.trim().length() > 160) {
				sms.sendMultipartTextMessage(temp, null, parts, sentIntents,
						deliveryIntents);
				
			} else if ((temp.length() > 0 && temp.length() < 2)
					&& (messageS.trim().length() > 0 && messageS.trim()
							.length() <= 160)) {
				sms.sendTextMessage(phoneN, null, messageS, sentPI, deliveredPI);
		
			} else if ((temp.length() > 0 && temp.length() < 2)
					&& (messageS.trim().length() > 160)) {
				sms.sendMultipartTextMessage(phoneN, null, parts, sentIntents,
						deliveryIntents);
		
			} else if ((temp.length() >= 2)
					&& (messageS.trim().length() > 0 && messageS.trim()
							.length() <= 160)) {
				sms.sendTextMessage(temp, null, messageS, sentPI, deliveredPI);
				} else {

				Toast.makeText(getActivity().getBaseContext(),
						"Please enter both phone number and message.",
						Toast.LENGTH_SHORT).show();
			}
			
		}
	}

}
