package com.example.smsforeva.smsoutbox;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD_Outbox;
import com.example.smsforeva.database.Outbox;

public class SmsListViewOutboxFragment extends Fragment {
	private View rootView;
	private SimpleCursorAdapter adapter;
	TextView lblMsg, lblNo;
	ArrayAdapter<String> adapter2;
	List<Outbox>values;
	//SimpleAdapter<Outbox>outboxs;
	CRUD_Outbox crud_c;
	ListView lvMsg2;
	public ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();

	@Override
	@SuppressWarnings("deprecation")
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_smsoutbox_listoutbox,
				container, false);
		lvMsg2 = (ListView) rootView.findViewById(R.id.lvMsg2);
		Uri sentURI = Uri.parse("content://sms/sent");
		String[] reqCols = new String[] { "_id", "address", "body" };
/*		ContentResolver cr = getActivity().getBaseContext()
				.getContentResolver();
		Cursor c = cr.query(sentURI, reqCols, null, null, null);
		adapter = new SimpleCursorAdapter(getActivity().getBaseContext(),
				R.layout.row2, c, new String[] { "address","body", "_id" },
				new int[] { R.id.lblNumber2,R.id.lblMsg2, R.id.lblId2 });*/
		oslist = new ArrayList<HashMap<String, String>>();
		crud_c = new CRUD_Outbox(getActivity().getBaseContext());
		crud_c.open();
		values = crud_c.getAllOutbox();
		for (int i = 0; i < values.size(); i++) {
			long id = values.get(i).getId();
			String address =values.get(i).getNotelepon();
			String mesage = values.get(i).getMessage();
			String idmes = values.get(i).getIdmes();
			Long timestamp = Long.parseLong(idmes);
			Calendar calendar =Calendar.getInstance();
			calendar.setTimeInMillis(timestamp);
			Date finalDate = calendar.getTime();
			String smsdate = finalDate.toString();
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("_id",id +"");
			map.put("address", address);
			map.put("body", mesage);
			map.put("date", smsdate);
			oslist.add(map);
			crud_c.close();
		}
		ListAdapter la = new SimpleAdapter(getActivity().getBaseContext(), oslist,R.layout.row, new String[] {
			"address", "body", "_id", "date" }, new int[] {
			R.id.lblNumber, R.id.lblMsg, R.id.lblId, R.id.lbltime });
		
		lvMsg2.setAdapter(la);
		lvMsg2.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long id) {
				// TODO Auto-generated method stub
	/*			String a = ((TextView)view.findViewById(R.id.lblMsg2)).getText().toString();
				String b = ((TextView)view.findViewById(R.id.lblNumber2)).getText().toString();
				String c = ((TextView)view.findViewById(R.id.lblId2)).getText().toString();
				Bundle bundle = new Bundle();
				bundle.putString("mesage", a);
				bundle.putString("number", b);
				bundle.putString("id", c);
				SmsViewOutboxFragment svif = new SmsViewOutboxFragment();
				svif.setArguments(bundle);
				android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.frame_container_smsoutbox, svif);
				ft.addToBackStack(null);
				ft.commit();*/
			}
		});
		return rootView;
	}
}
