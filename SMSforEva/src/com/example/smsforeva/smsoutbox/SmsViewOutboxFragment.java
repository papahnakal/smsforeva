package com.example.smsforeva.smsoutbox;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.smsforeva.R;

public class SmsViewOutboxFragment extends Fragment{
	private View rootView;
	String nomortelpon,namakontak,code;
	String mesage,number,id;
	TextView ta,tb,tc;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_smsoutbox_viewoutbox, container, false);
		Bundle bundle = this.getArguments();
		if (bundle != null) {
			if (bundle.containsKey("mesage")) {
			String	s = bundle.getString("mesage");
				mesage=s;
			}
			if (bundle.containsKey("number")) {
			String	n = bundle.getString("number");
				number =n;
			}
			if (bundle.containsKey("id")) {
				String	n = bundle.getString("id");
					id =n;
				}
		}
		ta = (TextView)rootView.findViewById(R.id.lblVbody2);
		tb = (TextView)rootView.findViewById(R.id.lblVaddress2);
		tc = (TextView)rootView.findViewById(R.id.lblVid2);
		
		ta.setText(mesage);
		tb.setText(number);
		tc.setText(id);
		/*String[] parts = mesage.split(" ");
		String part1 = parts[0];
		String part2 = parts[1];
		String part3 = parts[2];
		if (part1.equalsIgnoreCase("lha")){
			Toast.makeText(getActivity(), part1+part2+part3, Toast.LENGTH_SHORT).show();
		}*/
		 return rootView;
		 }
}
