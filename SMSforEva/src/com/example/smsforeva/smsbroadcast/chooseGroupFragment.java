package com.example.smsforeva.smsbroadcast;

import java.util.List;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Contact;
import com.example.smsforeva.database.Group;
import com.example.smsforeva.fragments.BroadcastFragment;


public class chooseGroupFragment extends Fragment{
	private View rootView;
	ListView list;
	TextView findgroup;
	ArrayAdapter<Group> adapter;
	CRUD crud;
	List<Group> values;
	Button ok,back,open;
	String query;
	String no ="";
	String na ="";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_sms_choosegroup, container, false);
		 back = (Button)rootView.findViewById(R.id.btn_getBackG);
		 back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				smsBroadcastFragment sf = new smsBroadcastFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.frame_container_broadcast, sf);
				transaction.commit();	
			}
		});
		 open = (Button)rootView.findViewById(R.id.btn_openGroup);
	
		 	list = (ListView) rootView.findViewById(R.id.listview_choosegroup);
			
			crud = new CRUD(getActivity().getBaseContext());
			crud.open();
			values = crud.getAllGroup();
			adapter = new ArrayAdapter<Group>(getActivity().getBaseContext(), android.R.layout.simple_list_item_multiple_choice,values);
			list.setAdapter(adapter);
			list.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
			list.setTextFilterEnabled(true);
			adapter.notifyDataSetChanged();
			crud.close();
			 open.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
					 try {
						CRUD_contact cruds = new CRUD_contact(getActivity().getBaseContext());
						cruds.openn();
						if(query.endsWith(",")){
							query =query.substring(0, query.length()-1);
							List<Contact>val =cruds.findnumber(query);
							cruds.close();
							if(val.size()<1){
								Toast.makeText(getActivity(), "No result!", Toast.LENGTH_SHORT).show();
							}else{
								//Toast.makeText(getActivity(), "have result", Toast.LENGTH_SHORT).show();
								Bundle bundle = new Bundle();
								bundle.putString("query", query);
								chooseGroupContactFragment bf = new chooseGroupContactFragment();
								bf.setArguments(bundle);
								FragmentTransaction transaction = getFragmentManager().beginTransaction();
								transaction.replace(R.id.frame_container_broadcast, bf);
								//transaction.addToBackStack(null);
								transaction.commit();
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(getActivity(), "please choose one or many group!", Toast.LENGTH_SHORT).show();
					}	
					}
				});
			ok = (Button)rootView.findViewById(R.id.btn_getGrp);
			ok.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					String num="";
					String nam="";
					CRUD_contact cruds =new CRUD_contact(getActivity().getBaseContext());
					try {
						cruds.openn();
						if(query.endsWith(",")){
							query =query.substring(0,query.length()-1);
							List<Contact>val =cruds.findnumber(query);
							for (int i = 0; i < val.size(); i++) {
								String n = val.get(i).getPhone();
								String m = val.get(i).getName_contact();
								num = num+n+";";
								nam = nam+m+";";
								no =num;
								na =nam;
							}
						}
						cruds.close();
					} catch (Exception e) {
						//Toast.makeText(getActivity().getBaseContext(), "Please choose group!", Toast.LENGTH_SHORT).show();
						// TODO: handle exception
					}
					
					Bundle bundle = new Bundle();
					bundle.putString("no", no);
					bundle.putString("na", na);
					FragmentManager fm=getActivity().getFragmentManager();
					
					if(fm.getBackStackEntryCount()>0){
						fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
					}
						smsBroadcastFragment ssf = new smsBroadcastFragment();
						BroadcastFragment bf = new BroadcastFragment();
						bf.setArguments(bundle);
						FragmentTransaction transaction = getFragmentManager().beginTransaction();
						transaction.replace(R.id.frame_container_broadcast, bf);
						//transaction.addToBackStack(null);
						transaction.remove(ssf);
						transaction.commit();
					
				
				/*	Intent tent = new Intent(choosegroup.this,broadcastsms.class);
					tent.putExtra("no", no);
					tent.putExtra("na", na);
					startActivity(tent);*/
					//Toast.makeText(getActivity().getBaseContext(), no, Toast.LENGTH_SHORT).show();
					}
			});
			findgroup = (TextView)rootView.findViewById(R.id.groupSrc);
			findgroup.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
					// TODO Auto-generated method stub
					chooseGroupFragment.this.adapter.getFilter().filter(arg0);
				}
				
				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
						int arg3) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable arg0) {
					// TODO Auto-generated method stub
					
				}
			});
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
/*				broadcastsms bcs = new broadcastsms();
				bcs.finish();*/
				// TODO Auto-generated method stub
				String id_g="";
				int len = parent.getCount();
				SparseBooleanArray checked =list.getCheckedItemPositions();
				for (int i = 0; i < len; i++) {
					if(checked.get(i)){
						int i_g = (int) values.get(i).getId();
						id_g=id_g+i_g+",";
						
					}
				}//Toast.makeText(getActivity().getBaseContext(), query, Toast.LENGTH_SHORT).show();
				query =id_g;
			}
		});
		
	return rootView;	 
	}
}
