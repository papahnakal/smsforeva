package com.example.smsforeva.smsbroadcast;

import java.util.List;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.smsforeva.R;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Contact;
import com.example.smsforeva.fragments.BroadcastFragment;

public class chooseGroupContactFragment extends Fragment{
	private View rootView;
	String query; 
	TextView findgroupcontact;
	ListView list;
	CRUD_contact cruds;
	List<Contact> values;
	String s="";
	String n="";
	ArrayAdapter<Contact> adapter;
	Button ok,back;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_sms_choosegroupcontact, container, false);
		 Bundle bundle =getArguments();
		 query =bundle.getString("query");
		 cruds =new CRUD_contact(getActivity().getBaseContext());
		 cruds.openn();
		 values =cruds.findnumber(query);
		 adapter =new ArrayAdapter<Contact>(getActivity(), android.R.layout.simple_list_item_multiple_choice,values);
		 //Toast.makeText(getActivity(), a, Toast.LENGTH_SHORT).show();
		 list = (ListView)rootView.findViewById(R.id.listview_choosegroupcontact);
		 list.setAdapter(adapter);
		 list.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
		 list.setTextFilterEnabled(true);
		 adapter.notifyDataSetChanged();
		 cruds.close();
		 back = (Button)rootView.findViewById(R.id.btn_getBackGC);
		 back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				smsBroadcastFragment sf = new smsBroadcastFragment();
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.frame_container_broadcast, sf);
				transaction.commit();	
			}
		});
		 ok = (Button)rootView.findViewById(R.id.btn_getGrpcontact);
		 ok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				try {
					Bundle bundle = new Bundle();
					bundle.putString("no", s);
					bundle.putString("na", n);
	FragmentManager fm=getActivity().getFragmentManager();
					
					if(fm.getBackStackEntryCount()>0){
						fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);	
					}
					smsBroadcastFragment sf = new smsBroadcastFragment();
					chooseGroupFragment cg = new chooseGroupFragment();
					BroadcastFragment bf = new BroadcastFragment();
					bf.setArguments(bundle);
					android.support.v4.app.FragmentTransaction ft =getFragmentManager().beginTransaction();
					ft.replace(R.id.frame_container_broadcast, bf);
					ft.remove(sf);
					ft.remove(cg);
					ft.commit();
				} catch (Exception e) {
					// TODO: handle exception
					
				}
				
			}
		});
		 list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				String ps="";
				String pn="";
				SparseBooleanArray checked = list.getCheckedItemPositions();
				int checkeditemscount = checked.size();
				for (int i = 0; i < checkeditemscount; i++) {
					int posisi = checked.keyAt(i);
					if(checked.valueAt(i)){
						String no = values.get(posisi).getPhone();
						String na =values.get(posisi).getName_contact();
						ps = ps+no+";";
						pn=pn+na+";";
					}
				}
				s=ps;
				n=pn;
			}
		});
		 findgroupcontact = (TextView)rootView.findViewById(R.id.groupcontactSrc);
		 findgroupcontact.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				chooseGroupContactFragment.this.adapter.getFilter().filter(arg0);
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		 return rootView;
	}
}
