package com.example.smsforeva.smsbroadcast;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.smsforeva.R;
import com.example.smsforeva.contact.listContactFragment;
import com.example.smsforeva.database.CRUD_contact;
import com.example.smsforeva.database.Group;

public class smsBroadcastFragment extends Fragment{
	private View rootView;
	IntentFilter sendIntentFilter, receiveIntentFilter;
	String SENT = "SMS_SENT";
	String DELIVERED = "SMS_DELIVERED";
	PendingIntent sentPI, deliveredPI;
	BroadcastReceiver smsSentReceiver, smsDeliveredReceiver;
	private static final String SMS_SEND_ACTION = "CTS_SMS_SEND_ACTION";
	private static final String SMS_DELIVERY_ACTION = "CTS_SMS_DELIVERY_ACTION";

	List<Group> values;
	ArrayAdapter<Group> groups;

	String na, no;
	Spinner spin, spin2;
	EditText number, message, name;
	Button getgroup, sendbroadcast;
	//String no,na;
	long ids;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView= inflater.inflate(R.layout.fragment_sms_broadcast, container, false);
		number = (EditText) rootView.findViewById(R.id.txt_phonegrop);
		message = (EditText) rootView.findViewById(R.id.txt_message_grop);
		name = (EditText) rootView.findViewById(R.id.txt_broadcast_group);
		Bundle bundle = this.getArguments();
		if (bundle != null) {
			if (bundle.containsKey("no")) {
			no = bundle.getString("no");
			number.setText(no);
				//number.setText(no);
			}
			if (bundle.containsKey("na")) {
			na = bundle.getString("na");
			name.setText(na);
			
			}
		}
		
		
		//name.setText(na);
		//number.setText(no);
		
		/*Intent inten = getIntent();
		no = inten.getStringExtra("no");
		na = inten.getStringExtra("na");*/
		sendIntentFilter = new IntentFilter(SMS_SEND_ACTION);
		receiveIntentFilter = new IntentFilter(SMS_DELIVERY_ACTION);
		getgroup = (Button) rootView.findViewById(R.id.btn_getGroup);
		getgroup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/*Intent inten = new Intent(broadcastsms.this, choosegroup.class);
				startActivity(inten);*/
				chooseGroupFragment ssf = new chooseGroupFragment();
				//ssf.setArguments(bundle);
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(R.id.frame_container_broadcast, ssf);
				//transaction.addToBackStack(null);
				transaction.commit();
			}
		});
		sentPI = PendingIntent.getBroadcast(getActivity(), 0, new Intent(SENT), 0);

		deliveredPI = PendingIntent.getBroadcast(getActivity(), 0,
				new Intent(DELIVERED), 0);
		sendbroadcast = (Button) rootView.findViewById(R.id.btn_send_broadcast);
		sendbroadcast.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String naGabung="";
				String noGabung="";
				String[]partna=name.getText().toString().split(";");	
				String[]partno = number.getText().toString().split(";");
				for (int i = 0; i < partna.length; i++) {
						 naGabung= naGabung+partna[i]+";"; 
				}
				 for (int o = 0; o < partna.length; o++) {
						noGabung = noGabung+partno[o]+";";
				}
				name.setText(naGabung);
				number.setText(noGabung);
				// TODO Auto-generated method stub
				String nomor = number.getText().toString();
				String pesan = message.getText().toString();
				if (nomor.equalsIgnoreCase("") && pesan.equalsIgnoreCase("")) {
					Toast.makeText(getActivity().getBaseContext(),
							"number phone and message text required!",
							Toast.LENGTH_SHORT).show();
				} else if(nomor.equalsIgnoreCase("")){
					Toast.makeText(getActivity().getBaseContext(),
							"number phone required!",
							Toast.LENGTH_SHORT).show();
				}else if(pesan.equalsIgnoreCase("")){
					Toast.makeText(getActivity().getBaseContext(),
							"message text required!",
							Toast.LENGTH_SHORT).show();
				}else {
					
					AlertDialog a =askoption();
					a.show();
				
				}
			}

			private AlertDialog askoption() {
				AlertDialog myQuittingDialogBox = new AlertDialog.Builder(getActivity())
				// set message, title, and icon
				.setTitle("Confirmation Send")
				.setMessage("Sending this message?")

				.setPositiveButton("Send",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								String nomor = number.getText().toString();
								String pesan = message.getText().toString();
								if (nomor.endsWith(";")) {
									nomor = nomor.substring(0, nomor.length() - 1);
									sendSMS(nomor, pesan);
									/*Toast.makeText(getActivity().getBaseContext(),
											"on sending progress, wait message delivered",
											Toast.LENGTH_SHORT).show();*/
								} else {
									sendSMS(nomor, pesan);
									
								
								}
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						}).create();
		return myQuittingDialogBox;

			}
		});

		number = (EditText) rootView.findViewById(R.id.txt_phonegrop);
		message = (EditText) rootView.findViewById(R.id.txt_message_grop);
		name = (EditText) rootView.findViewById(R.id.txt_broadcast_group);
		
		return rootView;	 
	}
	@Override
	public void onResume() {
		super.onResume();

		smsSentReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getActivity().getBaseContext(), "SMS sent",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(getActivity().getBaseContext(), "Generic failure",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(getActivity().getBaseContext(), "No service",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(getActivity().getBaseContext(), "Null PDU",
							Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(getActivity().getBaseContext(), "Radio off",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		};

		// create the BroadcastReceiver when the SMS is delivered
		smsDeliveredReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getActivity().getBaseContext(), "SMS delivered",
							Toast.LENGTH_SHORT).show();
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(getActivity().getBaseContext(), "SMS not delivered",
							Toast.LENGTH_SHORT).show();
					break;
				}
			}
		};

		// register the two BroadcastReceivers
		getActivity().registerReceiver(smsDeliveredReceiver, sendIntentFilter);
		getActivity().registerReceiver(smsSentReceiver, receiveIntentFilter);
	}

	@Override
	public void onPause() {
		super.onPause();
		// unregister the two BroadcastReceivers
		getActivity().unregisterReceiver(smsSentReceiver);
		getActivity().unregisterReceiver(smsDeliveredReceiver);
	}

	public void sendSMS(String phoneN, String messageS) {

		SmsManager sms = SmsManager.getDefault();
		ArrayList<String> parts = sms.divideMessage(messageS);
		ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
		ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();
		for (int i = 0; i < parts.size(); i++) {
			sentIntents.add(PendingIntent.getBroadcast(getActivity().getBaseContext(), 0,
					new Intent(SMS_SEND_ACTION), 0));
			deliveryIntents.add(PendingIntent.getBroadcast(getActivity().getBaseContext(), 0,
					new Intent(SMS_DELIVERY_ACTION), 0));
		}
		StringTokenizer st = new StringTokenizer(phoneN, ";");
		while (st.hasMoreElements()) {
			String temp = (String) st.nextElement();
			if (temp.length() > 0 && messageS.trim().length() > 160) {
				sms.sendMultipartTextMessage(temp, null, parts, sentIntents,
						deliveryIntents);
				Toast.makeText(getActivity().getBaseContext(),
						"on sending progress, wait message delivered",
						Toast.LENGTH_SHORT).show();
			if(temp.length() > 0 && messageS.trim().length() < 160){
				sms.sendMultipartTextMessage(temp, null, parts, sentIntents,
						deliveryIntents);
				Toast.makeText(getActivity().getBaseContext(),
						"on sending progress, wait message delivered",
						Toast.LENGTH_SHORT).show();
			}
			}else{
				Toast.makeText(getActivity().getBaseContext(),
						"Please enter both phone number and message.",
						Toast.LENGTH_SHORT).show();
			}
		}
		// sms.sendTextMessage(phoneN,null, messageS, sentPI, deliveredPI);
		/*sms.sendMultipartTextMessage(phoneN, null, parts, sentIntents,
				deliveryIntents);*/

	}
/*	 public void onBackPressed() {
	        //Display alert message when back button has been pressed
	        back();
	        return;
	    }
	private void back() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(Intent.ACTION_MAIN);
     intent.addCategory(Intent.CATEGORY_HOME);
     intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
     startActivity(intent);
     getActivity().finish();
	}*/

}
