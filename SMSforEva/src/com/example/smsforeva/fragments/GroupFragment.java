package com.example.smsforeva.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smsforeva.R;
import com.example.smsforeva.group.listGroupFragment;

public class GroupFragment extends Fragment{
	private View rootView;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
			rootView= inflater.inflate(R.layout.fragment_group, container, false);
		/*	if (rootView.findViewById(R.id.frame_container_group)!=null) {
				 if (savedInstanceState != null) {
		                return;
		            }
			}*/
			listGroupFragment newFragment = new listGroupFragment();
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(R.id.frame_container_group, newFragment);
			//transaction.addToBackStack(null);
			transaction.commit();
			
	return rootView;	 
	}

}
