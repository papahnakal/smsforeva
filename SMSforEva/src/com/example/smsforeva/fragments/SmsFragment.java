package com.example.smsforeva.fragments;

import com.example.smsforeva.R;
import com.example.smsforeva.sms.smsSingleFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SmsFragment extends Fragment{
	private View rootView;
	String nomortelpon,namakontak;;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_sms, container, false);
		 Bundle bundle = this.getArguments();

			if (bundle != null) {
				if (bundle.containsKey("no")) {
				String	s = bundle.getString("no");
					nomortelpon=s;
				}
				if (bundle.containsKey("na")) {
				String	n = bundle.getString("na");
					namakontak =n;
				}
		
			}
			Bundle bundle2 = new Bundle();
			bundle2.putString("no", nomortelpon);
			bundle2.putString("na", namakontak);
			smsSingleFragment newFragment = new smsSingleFragment();
			newFragment.setArguments(bundle2);
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(R.id.frame_container_sms, newFragment);
			//transaction.addToBackStack(null);
			transaction.commit();
		 
		 return rootView;	 
	}
}
