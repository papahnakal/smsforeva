package com.example.smsforeva.fragments;

import com.example.smsforeva.R;
import com.example.smsforeva.smsoutbox.SmsListViewOutboxFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SmsOutboxFragment extends Fragment{
	private View rootView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_smsoutbox, container, false);
		 SmsListViewOutboxFragment slvif =new SmsListViewOutboxFragment();
		 android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
		 ft.replace(R.id.frame_container_smsoutbox, slvif);
		 ft.commit();
		 
		 return rootView;
		 }
}
