package com.example.smsforeva.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.smsforeva.R;
import com.example.smsforeva.smsinbox.SmsListViewInboxFragment;

public class SmsInboxFragment extends Fragment{

	private View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_smsinbox, container, false);
		 SmsListViewInboxFragment slvif =new SmsListViewInboxFragment();
		 android.support.v4.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
		 ft.replace(R.id.frame_container_smsinbox, slvif);
		 ft.commit();
		 
		 return rootView;
		 }
}
