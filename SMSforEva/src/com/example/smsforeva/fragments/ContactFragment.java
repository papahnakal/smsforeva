package com.example.smsforeva.fragments;

import com.example.smsforeva.R;
import com.example.smsforeva.contact.listContactFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ContactFragment extends Fragment{
	private View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 rootView= inflater.inflate(R.layout.fragment_contact, container, false);
		 listContactFragment newFragment = new listContactFragment();
			FragmentTransaction transaction = getFragmentManager().beginTransaction();
			transaction.replace(R.id.frame_container_contact, newFragment);
			//transaction.addToBackStack(null);
			transaction.commit();
	return rootView;	 
	}
}
