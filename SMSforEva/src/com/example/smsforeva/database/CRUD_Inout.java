package com.example.smsforeva.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CRUD_Inout {
	private SQLiteDatabase db;
	private SQLHelper dbHelper;
	private String[]inoutcolumns ={
		SQLHelper.ID_INOUT,SQLHelper.KEY_ID_M,SQLHelper.KEY_MESSAGE_M,
		SQLHelper.KEY_NUMBER_M,SQLHelper.KEY_STATUS_M,SQLHelper.KEY_TIME_M
	};
	private String[]justmessage={
			SQLHelper.KEY_MESSAGE_M
	};
	public CRUD_Inout(Context context){
		dbHelper = new SQLHelper(context);
	}
	public void open(){
		db = dbHelper.getWritableDatabase();
	}
	public void close(){
		dbHelper.close();
	}
	public Message deleteMessage(long b){
		db.delete(SQLHelper.TABLE_INOUT, SQLHelper.ID_INOUT+" = "+b , null);
		Message newMessage = new Message();
		return newMessage;
	}
	public List<Message>findnumber(String M_number){
		List<Message>messages = new ArrayList<Message>();
		String table = SQLHelper.TABLE_INOUT;
		String index = SQLHelper.ID_INOUT;
		String id_m = SQLHelper.KEY_ID_M;
		String mes_m = SQLHelper.KEY_MESSAGE_M;
		String num_m = SQLHelper.KEY_NUMBER_M;
		String status_m =SQLHelper.KEY_STATUS_M;
		String time_m =SQLHelper.KEY_TIME_M;
		String findquery="Select * from "+table+" where "+num_m+" = '"+M_number+"' order by "+index+" asc";
		Log.e("log",findquery);
		Cursor c =db.rawQuery(findquery,null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Message message = new Message();
			message.setId(c.getLong(c.getColumnIndex(SQLHelper.ID_INOUT)));
			message.setIdmes(c.getInt(c.getColumnIndex(SQLHelper.KEY_ID_M)));
			message.setMessage(c.getString(c.getColumnIndex(SQLHelper.KEY_MESSAGE_M)));
			message.setNotelepon(c.getString(c.getColumnIndex(SQLHelper.KEY_NUMBER_M)));
			message.setStatus(c.getString(c.getColumnIndex(SQLHelper.KEY_STATUS_M)));
			message.setTime(c.getString(c.getColumnIndex(SQLHelper.KEY_TIME_M)));
			messages.add(message);
			c.moveToNext();
		}
		c.close();
		/*if (c.moveToFirst()) {
			do{
				Message message = new Message();
				message.setId(c.getLong(c.getColumnIndex(SQLHelper.ID_INOUT)));
				message.setIdmes(c.getString(c.getColumnIndex(SQLHelper.KEY_ID_M)));
				message.setMessage(c.getString(c.getColumnIndex(SQLHelper.KEY_MESSAGE_M)));
				message.setNotelepon(c.getString(c.getColumnIndex(SQLHelper.KEY_NUMBER_M)));
				message.setStatus(c.getString(c.getColumnIndex(SQLHelper.KEY_STATUS_M)));
				message.setTime(c.getInt(c.getColumnIndex(SQLHelper.KEY_TIME_M)));
				messages.add(message);
			}while(c.moveToNext());
		}*/
		return messages;
	}
	public Message createMessage(String inout_message,String inout_number,
			String inout_status,String inout_time){
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_MESSAGE_M, inout_message);
		values.put(SQLHelper.KEY_NUMBER_M, inout_number);
		values.put(SQLHelper.KEY_STATUS_M, inout_status);
		values.put(SQLHelper.KEY_TIME_M, inout_time);
		long insertMessage = db.insert(SQLHelper.TABLE_INOUT, null, values);
		Cursor cr = db.query(SQLHelper.TABLE_INOUT, inoutcolumns, SQLHelper.ID_INOUT+" = "+insertMessage, null, null, null, SQLHelper.ID_INOUT);

		cr.moveToFirst();
		Message newMessage = cursorToMessage(cr);
		cr.close();
		return newMessage;
	}
	public Message createMessageIn(int id_m,String inout_message,String inout_number,
			String inout_status,String inout_time){
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_ID_M, id_m);
		values.put(SQLHelper.KEY_MESSAGE_M, inout_message);
		values.put(SQLHelper.KEY_NUMBER_M, inout_number);
		values.put(SQLHelper.KEY_STATUS_M, inout_status);
		values.put(SQLHelper.KEY_TIME_M, inout_time);
		long insertMessage = db.insert(SQLHelper.TABLE_INOUT, null, values);
		Cursor cr = db.query(SQLHelper.TABLE_INOUT, inoutcolumns, SQLHelper.ID_INOUT+" = "+insertMessage, null, null, null, SQLHelper.ID_INOUT);

		cr.moveToFirst();
		Message newMessage = cursorToMessage(cr);
		cr.close();
		return newMessage;
	}
	public List<Message>getAllMessage(){
		List<Message> messages = new ArrayList<Message>();
		Cursor c = db.query(SQLHelper.TABLE_INOUT, inoutcolumns, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Message message = cursorToMessage(c);
			messages.add(message);
			c.moveToNext();
		}
		c.close();
		return messages;
	}
	private Message cursorToMessage(Cursor cr) {
		// TODO Auto-generated method stub
		Message message = new Message();
		message.setId(cr.getLong(0));
		message.setIdmes(cr.getInt(1));
		message.setMessage(cr.getString(2));
		message.setNotelepon(cr.getString(3));
		message.setStatus(cr.getString(4));
		message.setTime(cr.getString(5));
		return message;
	}
	private Message cursorJustMessage(Cursor cr){
		Message message = new Message();
		message.setMessage(cr.getString(0));
		return message;
	}
}
