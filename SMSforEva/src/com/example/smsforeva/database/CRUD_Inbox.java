package com.example.smsforeva.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CRUD_Inbox {
	private SQLiteDatabase db;
	private SQLHelper dbHelper;
	private String[] showInbox = { SQLHelper.ID_INBOX,
			SQLHelper.KEY_ID_INBOX,SQLHelper.KEY_MESSAGE_INBOX, SQLHelper.KEY_NUMBER_INBOX };
	private String[]listInbox={
			SQLHelper.KEY_NUMBER_INBOX
	};
	public CRUD_Inbox(Context context){
		dbHelper = new SQLHelper(context);
	}
	public void open()throws SQLException{
		db=dbHelper.getWritableDatabase();
	}
	public void close(){
		dbHelper.close();
	}
	public Inbox createInbox(String id,String message,String nope){
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_ID_INBOX, id);
		values.put(SQLHelper.KEY_MESSAGE_INBOX, message);
		values.put(SQLHelper.KEY_NUMBER_INBOX, nope);
		long insertID =db.insert(SQLHelper.TABLE_INBOX, null, values);
		Cursor c = db.query(SQLHelper.TABLE_INBOX, showInbox, SQLHelper.ID_INBOX+"="+insertID, null, null, null, null);
		c.moveToFirst();
		Inbox newInbox =cursorToInbox(c);
		c.close();
		return newInbox;
	}
	/*public List<Inbox>findnumber(String number){
		List<Inbox>inboxs = new ArrayList<Inbox>();
		String table = SQLHelper.TABLE_INBOX;
		String idinbox = SQLHelper.KEY_ID_INBOX;
		String nomor = SQLHelper.KEY_NUMBER_INBOX;
		String 
	}*/
	public Inbox deleteInbox(long b){
		db.delete(SQLHelper.TABLE_INBOX, SQLHelper.ID_INBOX+"="+b,null);
		Inbox newInbox =new Inbox();
		return newInbox;
	}
	public List<Inbox>getAllInbox(){
		List<Inbox>inboxs = new ArrayList<Inbox>();
		Cursor c = db.query(SQLHelper.TABLE_INBOX, showInbox, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Inbox inbox = cursorToInbox(c);
			inboxs.add(inbox);
			c.moveToNext();
		}
		c.close();
		return inboxs;
	}
	public List<Inbox>getListnumber(){
		List<Inbox>inboxs = new ArrayList<Inbox>();
		Cursor c = db.query(SQLHelper.TABLE_INBOX, listInbox, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Inbox inbox = cursorToLisyInbox(c);
			inboxs.add(inbox);
			c.moveToNext();
		}c.close();
			return inboxs;
		
	}
	private Inbox cursorToLisyInbox(Cursor c) {
		// TODO Auto-generated method stub
		Inbox inbox = new Inbox();
		inbox.setId(c.getLong(0));
		inbox.setNotelepon(c.getString(1));
		return inbox;
		
	}
	private Inbox cursorToInbox(Cursor c) {
		// TODO Auto-generated method stub
		Inbox inbox = new Inbox();
		inbox.setId(c.getLong(0));
		inbox.setIdmes(c.getString(1));
		inbox.setMessage(c.getString(2));
		inbox.setNotelepon(c.getString(3));
		return inbox;
	}
}
