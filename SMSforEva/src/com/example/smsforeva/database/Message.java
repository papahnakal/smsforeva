package com.example.smsforeva.database;

public class Message {
private long id;
private int idmes;
private String notelepon;
private String message;
private String time;
private String status;
public Message(){}
public Message(long id, int idmes, String notelepon, String message,
		String time,String status) {
	super();
	this.id = id;
	this.idmes = idmes;
	this.notelepon = notelepon;
	this.message = message;
	this.time = time;
	this.status = status;
}

public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public int getIdmes() {
	return idmes;
}
public void setIdmes(int idmes) {
	this.idmes = idmes;
}
public String getNotelepon() {
	return notelepon;
}
public void setNotelepon(String notelepon) {
	this.notelepon = notelepon;
}
/*
-bikin database yg ada kolom status kirim atau terima
 -jika kirim bg jadi ijo klo terima kuning
 
 -ketika pencet slh satu message inbox periksa ada no telepon atau tidak
 -jika ada ditampilkan bdasarkan field time lalu baru add message baru kalo tidak add baru
 
 -
*/
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
public String getTime() {
	return time;
}
public void setTime(String time) {
	this.time = time;
}
@Override
public String toString() {
	return message;
}
}
