package com.example.smsforeva.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CRUD_Outbox {
	private SQLiteDatabase db;
	private SQLHelper dbHelper;
	private String[] showoutbox = { SQLHelper.ID_OUTBOX,
			SQLHelper.KEY_ID_OUTBOX, SQLHelper.KEY_MESSAGE_OUTBOX,
			SQLHelper.KEY_NUMBER_OUTBOX };

	private String[]ListOutbox={
			 SQLHelper.ID_OUTBOX,
			SQLHelper.KEY_NUMBER_OUTBOX
	};
	public CRUD_Outbox(Context context){
		dbHelper = new SQLHelper(context);
	}
	public void open()throws SQLException{
		db = dbHelper.getWritableDatabase();
	}
	public void close(){
		dbHelper.close();
	}
	public Outbox deleteOutbox(long b){
		db.delete(SQLHelper.TABLE_OUTBOX, SQLHelper.ID_OUTBOX+"="+b, null);
		Outbox outbox = new Outbox();
		return outbox;
	}
	public Outbox createOutbox(String id,String message,String nope){
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_ID_OUTBOX, id);
		values.put(SQLHelper.KEY_MESSAGE_OUTBOX, message);
		values.put(SQLHelper.KEY_NUMBER_OUTBOX, nope);
		long insertID = db.insert(SQLHelper.TABLE_OUTBOX, "", values);
		Cursor c = db.query(SQLHelper.TABLE_OUTBOX, showoutbox, SQLHelper.ID_OUTBOX+"="+insertID, null, null, null, null);
		c.moveToFirst();
		Outbox newOutbox = cursorToOutbox(c);
		c.close();		
		return newOutbox;
	}
	public List<Outbox>getAllOutbox(){
		List<Outbox>outboxs = new ArrayList<Outbox>();
		Cursor  c = db.query(SQLHelper.TABLE_OUTBOX, showoutbox, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Outbox outbox = cursorToOutbox(c);
			outboxs.add(outbox);
			c.moveToNext();
		}
		c.close();	
		return outboxs;
	}
	public List<Outbox>getListOutbox(){
		List<Outbox>outboxs = new ArrayList<Outbox>();
		Cursor c =db.query(SQLHelper.TABLE_OUTBOX, ListOutbox, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Outbox outbox = cursorToList(c);
			outboxs.add(outbox);
			c.moveToNext();
		}
		c.close();
		return outboxs;
	}
	private Outbox cursorToList(Cursor c) {
		// TODO Auto-generated method stub
		Outbox outbox = new Outbox();
		outbox.setId(c.getLong(0));
		outbox.setNotelepon(c.getString(1));
		return outbox;
	}
	private Outbox cursorToOutbox(Cursor c) {
		// TODO Auto-generated method stub
		Outbox outbox  = new Outbox();
		outbox.setId(c.getLong(0));
		outbox.setIdmes(c.getString(1));
		outbox.setMessage(c.getString(2));
		outbox.setNotelepon(c.getString(3));
		return outbox;
	}
}