package com.example.smsforeva.database;

public class Inbox {
	private long id;
	private String idmes;
	private String notelepon;
	private String message;
	
	public Inbox(){}
	public Inbox(long id, String idmes,String notelepon, String message) {
		super();
		
		this.id = id;
		this.idmes = idmes;
		this.notelepon = notelepon;
		this.message = message;
	}
	
	public String getIdmes() {
		return idmes;
	}
	public void setIdmes(String idmes) {
		this.idmes = idmes;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNotelepon() {
		return notelepon;
	}
	public void setNotelepon(String notelepon) {
		this.notelepon = notelepon;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return  notelepon;
	}
	
}
