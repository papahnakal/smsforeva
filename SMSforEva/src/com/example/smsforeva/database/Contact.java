package com.example.smsforeva.database;

public class Contact{
	private long id;
	private int group_id;
	private String name_contact;
	private String phone;
	private String position;
	private String address;
	private String note_contact;
	private String code;
	private String name;
	private String note;
	private boolean selected; 
	
	public Contact(){}
	public Contact(String name_contact) {
		super();
		this.name_contact = name_contact;
		selected =false;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getGroup_id() {
		return group_id;
	}
	public void setGroup_id(int group_id) {
		this.group_id = group_id;
	}
	public String getName_contact() {
		return name_contact;
	}
	public void setName_contact(String name_contact) {
		this.name_contact = name_contact;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNote_contact() {
		return note_contact;
	}
	public void setNote_contact(String note_contact) {
		this.note_contact = note_contact;
	}
	@Override
	public String toString() {
		return name+ " - " +name_contact;
	}
	

}