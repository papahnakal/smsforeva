package com.example.smsforeva.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CRUD {
	private SQLiteDatabase db;
	private SQLHelper dbHelper;
	/*private String[] contactColumns = { SQLHelper.KEY_ID,SQLHelper.KEY_NAME,
			SQLHelper.KEY_PHONE_NUMBER, SQLHelper.KEY_POSITION,
			SQLHelper.KEY_ADDRESS, SQLHelper.KEY_NOTE };*/
	private String[] groupColumns = {SQLHelper.ID_GROUP,SQLHelper.KEY_CODE,
			SQLHelper.KEY_NAME_GROUP,SQLHelper.KEY_NOTE_GROUP};
	private String[] GroupNameColumns ={SQLHelper.ID_GROUP,SQLHelper.KEY_NAME_GROUP};
	/*private String[] contactGroupColumns = {SQLHelper.KEY_ID,SQLHelper.KEY_GROUP_ID,
			SQLHelper.KEY_NAME,SQLHelper.KEY_PHONE_NUMBER, SQLHelper.KEY_POSITION,
			SQLHelper.KEY_ADDRESS, SQLHelper.KEY_NOTE };*/
	public CRUD(Context contex){
		dbHelper = new SQLHelper(contex);
	}
	public void open()throws SQLException{
		db = dbHelper.getWritableDatabase();
	}
	public void close(){
		dbHelper.close();
	}
	public List<Group> findGroup(String code){
		List<Group> groups = new ArrayList<Group>();
		String tabel = SQLHelper.TABLE_GROUP;
		String codegroup =SQLHelper.KEY_CODE;
		String findQuery = "Select * from "+ tabel+" where "+codegroup+" like'%"+code+"%'"+ " ORDER BY " + codegroup + " ASC"; 
		Log.e("log",findQuery );
		Cursor c = db.rawQuery(findQuery, null);
		if (c.moveToFirst()){
			do{
			Group group = new Group();
			group.setId(c.getInt(c.getColumnIndex(SQLHelper.ID_GROUP)));
			group.setCode(c.getString(c.getColumnIndex(SQLHelper.KEY_CODE)));
			group.setName(c.getString(c.getColumnIndex(SQLHelper.KEY_NAME_GROUP)));
			group.setNote(c.getString(c.getColumnIndex(SQLHelper.KEY_NOTE_GROUP)));
		
			groups.add(group);
		}while(c.moveToNext());
		}
			return groups;
	}
	public Group createGroup(String code,String name,String note){
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_CODE, code);
		values.put(SQLHelper.KEY_NAME_GROUP, name);
		values.put(SQLHelper.KEY_NOTE_GROUP, note);
		long insertID = db.insert(SQLHelper.TABLE_GROUP, null, values);
		Cursor c = db.query(SQLHelper.TABLE_GROUP, groupColumns, SQLHelper.ID_GROUP+"="+insertID, null, null, null, null);
		c.moveToFirst();
		Group newGroup = cursorToGroup(c);
		c.close();
		return newGroup;
		
	}public Group deleteGroup(long b){
		
		db.delete(SQLHelper.TABLE_GROUP, SQLHelper.ID_GROUP+" = "+b,null);
		Group newGroup =new Group();
		return newGroup;
	}public Contact updateGroupContact(int b,int ganti,String code,String name){
		
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_ID_GROUP, ganti);
		values.put(SQLHelper.KEY_CODE, code);
		values.put(SQLHelper.KEY_NAME_GROUP, name);
		db.update(SQLHelper.TABLE_CONTACT, values, SQLHelper.KEY_ID_GROUP+"="+b, null);
		Contact contact = new Contact();
		return contact;
	}
	public Group updateGroup(long id,String code,String name,String note){
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_CODE, code);
		values.put(SQLHelper.KEY_NAME_GROUP,name);
		values.put(SQLHelper.KEY_NOTE_GROUP, note);
		db.update(SQLHelper.TABLE_GROUP,values, SQLHelper.ID_GROUP+"="+id, null);
		//db.update(SQLHelper.TABLE_GROUP, values, SQLHelper.KEY_ID+" = "+id, groupColumns);
		Group group = new Group();
		return group;
	}
	
	public List<Group> getAllGroup(){
		List<Group>groups = new ArrayList<Group>();
		Cursor c =db.query(SQLHelper.TABLE_GROUP, groupColumns, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Group group = cursorToGroup(c);
			groups.add(group);
			c.moveToNext();
		}
		c.close();
		return groups;
	}
	
	private Group cursorToGroup(Cursor c) {
		Group group = new Group();
		group.setId(c.getLong(0));
	    group.setCode(c.getString(1));
		group.setName(c.getString(2));
	    group.setNote(c.getString(3));
		// TODO Auto-generated method stub
		return group;
	}
	public List<Group>getAllnames(){
		List<Group>groups = new ArrayList<Group>();
		Cursor c =db.query(SQLHelper.TABLE_GROUP, GroupNameColumns, null, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast()){
			Group group = cursorToName(c);
			groups.add(group);
			c.moveToNext();
		}
		c.close();
		return groups;
	}
	private Group cursorToName(Cursor c) {
		Group group = new Group();
		group.setId(c.getLong(0));
		group.setName(c.getString(1));
		// TODO Auto-generated method stub
		return group;
	}
	
}
