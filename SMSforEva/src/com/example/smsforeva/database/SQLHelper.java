package com.example.smsforeva.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper {
	private static final String LOG = "DatabaseHelper";
	private static final int DATABASE_VERSION = 1;
	// database name
	private static final String DB_NAME = "DBSMSapp";
	// tabel name
	public static final String TABLE_GROUP = "groupsms2";
	public static final String TABLE_CONTACT = "contacts2";
	public static final String TABLE_INBOX = "inbox2";
	public static final String TABLE_OUTBOX = "outbox2";
	public static final String TABLE_INOUT="inout";
	// column name common
	public static final String ID_CONTACT = "id";
	public static final String ID_GROUP = "id";
	public static final String ID_INBOX = "id";
	public static final String ID_OUTBOX = "id";
	public static final String ID_INOUT = "id";
	// COLLUMN OUTBOX
	public static final String KEY_ID_OUTBOX = "idmes";
	public static final String KEY_MESSAGE_OUTBOX = "message";
	public static final String KEY_NUMBER_OUTBOX = "notelepon";
	// coullumn inbox
	public static final String KEY_ID_INBOX = "idmes";
	public static final String KEY_MESSAGE_INBOX = "message";
	public static final String KEY_NUMBER_INBOX = "notelepon";
	//collumn inout
	public static final String KEY_ID_M ="idmes";
	public static final String KEY_MESSAGE_M="message";
	public static final String KEY_NUMBER_M="notelepon";
	public static final String KEY_STATUS_M="status";
	public static final String KEY_TIME_M="time";
	// collumn group tabel
	public static final String KEY_CODE = "code";
	public static final String KEY_NAME_GROUP = "name";
	public static final String KEY_NOTE_GROUP = "note";
	// contact
	public static final String KEY_ID_GROUP = "group_id";
	public static final String KEY_NAME_CONTACT = "name_contact";
	public static final String KEY_PHONE_NUMBER = "phone";
	public static final String KEY_PPOSITION = "position";
	public static final String KEY_ADDRESS = "address";
	public static final String KEY_NOTE_CONTACT = "note_contact";
	// group table statement
	private static final String CREATE_TABLE_INOUT ="create table " 
			+ TABLE_INOUT + "(" + ID_INOUT
			+ " integer primary key autoincrement, " + KEY_ID_M
			+ " integer unique, " + KEY_MESSAGE_M + " text, " + KEY_NUMBER_M
			+ " text, " + KEY_STATUS_M + " text, " + KEY_TIME_M
			+ " text);";
	private static final String CREATE_TABLE_OUTBOX = "create table "
			+ TABLE_OUTBOX + "(" + ID_OUTBOX
			+ " integer primary key autoincrement, " + KEY_ID_OUTBOX
			+ " text, " + KEY_MESSAGE_OUTBOX + " text, " + KEY_NUMBER_OUTBOX
			+ " text);";
	private static final String CREATE_TABLE_INBOX = "create table "
			+ TABLE_INBOX + "(" + ID_INBOX
			+ " integer primary key autoincrement, " + KEY_ID_INBOX + " text, "
			+ KEY_MESSAGE_INBOX + " text, " + KEY_NUMBER_INBOX + " text);";
	private static final String CREATE_TABLE_GROUP = "create table "
			+ TABLE_GROUP + "(" + ID_GROUP
			+ " integer primary key autoincrement, " + KEY_CODE + " text, "
			+ KEY_NAME_GROUP + " text, " + KEY_NOTE_GROUP + " text);";
	private static final String CREATE_TABLE_CONTACT = "CREATE TABLE "
			+ TABLE_CONTACT + "(" + ID_CONTACT
			+ " integer primary key autoincrement, " + KEY_ID_GROUP
			+ " INTEGER, " + KEY_CODE + " TEXT, " + KEY_NAME_GROUP + " TEXT, "
			+ KEY_NAME_CONTACT + " TEXT, " + KEY_PHONE_NUMBER + " TEXT, "
			+ KEY_PPOSITION + " TEXT, " + KEY_ADDRESS + " TEXT, "
			+ KEY_NOTE_CONTACT + " TEXT);";

	/*
	 * SELECT c.customerNumber, c.customerName, orderNumber, o.status FROM
	 * customers c LEFT JOIN orders o ON c.customerNumber = o.customerNumber
	 */
	public SQLHelper(Context context) {
		super(context, DB_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		// db.execSQL(CREATE_TABLE_CONTACT);
		db.execSQL(CREATE_TABLE_GROUP);
		db.execSQL(CREATE_TABLE_CONTACT);
		db.execSQL(CREATE_TABLE_INBOX);
		db.execSQL(CREATE_TABLE_OUTBOX);
		db.execSQL(CREATE_TABLE_INOUT);
		// db.execSQL(CREATE_TABLE_CONTACT_GROUP);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// db.execSQL("DROP TABLE IF EXISTS"+TABLE_CONTACT);
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_GROUP);
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_CONTACT);
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_INBOX);
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_OUTBOX);
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_INOUT);
		// db.execSQL("DROP TABLE IF EXISTS"+TABLE_CONTACT_GROUP);
		onCreate(db);
	}

}
