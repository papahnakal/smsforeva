package com.example.smsforeva.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CRUD_contact {
	private SQLiteDatabase db;
	private SQLHelper dbHelper;
	private String[] contactColumns = { SQLHelper.ID_CONTACT,
			SQLHelper.KEY_ID_GROUP, SQLHelper.KEY_NAME_CONTACT,
			SQLHelper.KEY_PHONE_NUMBER, SQLHelper.KEY_PPOSITION,
			SQLHelper.KEY_ADDRESS, SQLHelper.KEY_NOTE_CONTACT };
	private String[] contactallColumns = { SQLHelper.ID_CONTACT,
			SQLHelper.KEY_ID_GROUP, SQLHelper.KEY_CODE,
			SQLHelper.KEY_NAME_GROUP, SQLHelper.KEY_NAME_CONTACT,
			SQLHelper.KEY_PHONE_NUMBER, SQLHelper.KEY_PPOSITION,
			SQLHelper.KEY_ADDRESS, SQLHelper.KEY_NOTE_CONTACT };

	public CRUD_contact(Context context) {
		dbHelper = new SQLHelper(context);
	}

	public void openn() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();

	}
public Contact deleteContact(long b){
	db.delete(SQLHelper.TABLE_CONTACT, SQLHelper.ID_CONTACT+" = "+b,null);
	Contact newContact =new Contact();
	return newContact;
}

	public Contact createContact(int group_id, String g_code, String g_name,
			String name, String phone, String position, String address,
			String note) {
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_ID_GROUP, group_id);
		values.put(SQLHelper.KEY_CODE, g_code);
		values.put(SQLHelper.KEY_NAME_GROUP, g_name);
		values.put(SQLHelper.KEY_NAME_CONTACT, name);
		values.put(SQLHelper.KEY_PHONE_NUMBER, phone);
		values.put(SQLHelper.KEY_PPOSITION, position);
		values.put(SQLHelper.KEY_ADDRESS, address);
		values.put(SQLHelper.KEY_NOTE_CONTACT, note);
		long insertContact = db.insert(SQLHelper.TABLE_CONTACT, null, values);
		Cursor ct = db.query(SQLHelper.TABLE_CONTACT, contactColumns,
				SQLHelper.ID_CONTACT + " = " + insertContact, null, null, null,
				null);
		ct.moveToFirst();
		Contact newcontact = cursorToContact(ct);
		ct.close();
		return newcontact;
	}

	public Contact updateContact(long id,int group_id, String g_code, String g_name,
			String name, String phone, String position, String address,
			String note){
		ContentValues values = new ContentValues();
		values.put(SQLHelper.KEY_ID_GROUP, group_id);
		values.put(SQLHelper.KEY_CODE, g_code);
		values.put(SQLHelper.KEY_NAME_GROUP, g_name);
		values.put(SQLHelper.KEY_NAME_CONTACT, name);
		values.put(SQLHelper.KEY_PHONE_NUMBER, phone);
		values.put(SQLHelper.KEY_PPOSITION, position);
		values.put(SQLHelper.KEY_ADDRESS, address);
		values.put(SQLHelper.KEY_NOTE_CONTACT, note);
		db.update(SQLHelper.TABLE_CONTACT, values, SQLHelper.ID_CONTACT+"="+id, null);
		Contact contact = new Contact();
		return contact;
	}
	public List<Contact> findnumber(String G_id){
		List<Contact>contacts = new ArrayList<Contact>();
		String table = SQLHelper.TABLE_CONTACT;
		String idgroup =SQLHelper.KEY_ID_GROUP;
		String numberP =SQLHelper.KEY_PHONE_NUMBER;
		String findquery ="Select * from "+table+" where "+idgroup+" in ("+G_id+") order by "+idgroup+" asc";
		Log.e("log",findquery);
		Cursor c =db.rawQuery(findquery, null);
		if(c.moveToFirst()){
			do{
				Contact contact = new Contact();
				contact.setId(c.getLong(c.getColumnIndex(SQLHelper.ID_CONTACT)));
				contact.setGroup_id(c.getInt(c.getColumnIndex(SQLHelper.KEY_ID_GROUP)));
				contact.setCode(c.getString(c.getColumnIndex(SQLHelper.KEY_CODE)));
				contact.setName(c.getString(c.getColumnIndex(SQLHelper.KEY_NAME_GROUP)));
				contact.setName_contact(c.getString(c.getColumnIndex(SQLHelper.KEY_NAME_CONTACT)));
				contact.setPhone(c.getString(c.getColumnIndex(SQLHelper.KEY_PHONE_NUMBER)));
				contact.setPosition(c.getString(c.getColumnIndex(SQLHelper.KEY_PPOSITION)));
				contact.setAddress(c.getString(c.getColumnIndex(SQLHelper.KEY_ADDRESS)));
				contact.setNote_contact(c.getString(c.getColumnIndex(SQLHelper.KEY_NOTE_CONTACT)));
				contacts.add(contact);
			}while(c.moveToNext());
		}
		return contacts;
	}

	private Contact cursorToContact(Cursor ct) {
		// TODO Auto-generated method stub
		Contact contact = new Contact();
		contact.setId(ct.getLong(0));
		contact.setGroup_id(ct.getInt(1));
		contact.setName_contact(ct.getString(2));
		contact.setPhone(ct.getString(3));
		contact.setPosition(ct.getString(4));
		contact.setAddress(ct.getString(5));
		contact.setNote_contact(ct.getString(6));
		return contact;
	}

	private Contact cursorToAllContact(Cursor ct) {
		// TODO Auto-generated method stub
		Contact contact = new Contact();
		contact.setId(ct.getLong(0));
		contact.setGroup_id(ct.getInt(1));
		contact.setCode(ct.getString(2));
		contact.setName(ct.getString(3));
		contact.setName_contact(ct.getString(4));
		contact.setPhone(ct.getString(5));
		contact.setPosition(ct.getString(6));
		contact.setAddress(ct.getString(7));
		contact.setNote_contact(ct.getString(8));
		return contact;
	}

	public List<Contact> getAllContact() {
		List<Contact> contacts = new ArrayList<Contact>();
		Cursor c = db.query(SQLHelper.TABLE_CONTACT, contactColumns, null,
				null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			Contact contact = cursorToContact(c);
			contacts.add(contact);
			c.moveToNext();
		}
		c.close();
		return contacts;
	}

	public List<Contact> getFull() {
		List<Contact> contacts = new ArrayList<Contact>();
		Cursor c = db.query(SQLHelper.TABLE_CONTACT, contactallColumns, null,
				null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			Contact contact = cursorToAllContact(c);
			contacts.add(contact);
			c.moveToNext();
		}
		c.close();
		return contacts;
	}

	

}
